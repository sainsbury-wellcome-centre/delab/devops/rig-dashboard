#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 2022.08.03 Chaofei Bao
import os
from datetime import timedelta

import dash
#import dash_auth
import dash_bootstrap_components as dash_bc
from dash import Input, Output, State, dcc, html, MATCH, dash_table
from dash.exceptions import PreventUpdate
from dash_extensions import EventListener
import dash_function as dfun
from dash_function import zmq_messenger as zmq_msg
import pandas as pd
import datetime as dt
from datetime import date
import dash_daq as daq
from dash_iconify import DashIconify
import re
from plotly.subplots import make_subplots
import plotly.express as px
import json

HOME = os.path.expanduser("~")

read_from_ceph = False

MSG = zmq_msg()
MSG.getPusherConnection()
app = dash.Dash(__name__, external_stylesheets=[dash_bc.themes.BOOTSTRAP]) #, server=server)
server = app.server

# this is the page refresh rate
refresh_rate = 2*1000
# this allows callback in another callback function
app.config['suppress_callback_exceptions'] = True

# make the rig monitor cards
def generate_rig_card(df,btn_lock):
    rig_name = df.iloc[0,0]
    rig_status = dfun.get_rig_status(df)
    subjid = df.iloc[0,9]
    sessid = df.iloc[0,7]
    protocol = df.iloc[0,13]
    num_trial = df.iloc[0,17]
    total_rew = df.iloc[0,18]
    viol = df.iloc[0,19]
    hits = df.iloc[0,20]
    time_start = pd.to_datetime(df.iloc[0,12])
    rig_color = 'danger'
    button_lb = 'test'
    button_cl = 'warning'
    time_passed_str = ''
    #time_start = dt.datetime.fromtimestamp(df.iloc[0,12]/1000)
    # if rig_name == 373103:
    #     print(df)
    delta_time = dt.datetime.now()-time_start
    days1, hours1, minutes1, seconds1 = delta_time.days, delta_time.seconds // 3600, delta_time.seconds // 60 % 60, delta_time.seconds % 60

    if rig_status == 'running':
        rig_color = 'success'
        button_lb = 'stop'
        button_cl = 'danger'
        time_passed_str = f'Time since start: {days1*24+hours1:02d}:{minutes1:02d}:{seconds1:02d}'
    elif rig_status == 'testing':
        rig_color = 'info'
        button_lb = 'stop'
        button_cl = 'danger'
        time_passed_str = f'Time since start: {days1*24+hours1:02d}:{minutes1:02d}:{seconds1:02d}'
    elif rig_status == 'stopped':
        rig_color = 'secondary'
        button_lb = 'test'
        button_cl = 'warning'
        stattime =  time_start = pd.to_datetime(df['stattime'].iloc[0])
        time_passed_str = f'Session Stopped at {stattime.strftime("%H:%M:%S")}'
    elif rig_status == 'broken':
        rig_color = 'danger'
        button_lb = 'test'
        button_cl = 'warning'
        time_passed_str = 'Rig Broken! Please check!'
    elif rig_status == 'ready':
        rig_color = 'secondary'
        button_lb = 'test'
        button_cl = 'warning'
        stattime =  time_start = pd.to_datetime(df['stattime'].iloc[0])
        time_passed_str = f'Session Stopped at {stattime.strftime("%H:%M:%S")}.ready'
    elif rig_status == 'start':
        rig_color = 'warning'
        button_lb = 'test'
        button_cl = 'warning'
        time_passed_str = 'Session Start'
    else:
        rig_color = 'danger'
        button_lb = 'test'
        button_cl = 'warning'
        time_passed_str = 'Session Error!!'
    
    
    if delta_time.total_seconds()//60>80:
        show_time = 80
        progress_animation = False
    else:
        show_time = delta_time.total_seconds()//60
        progress_animation = True
    
    rig_card = dash_bc.Card(
        [
            dash_bc.CardHeader([f'Rig: {rig_name}',
                                dash_bc.Button(button_lb,color = button_cl,
                                               className='float-end',
                                               id={'type':'rig_button','index':str(rig_name)},
                                               n_clicks = 0, disabled=(not btn_lock)),
                                ],className = 'align-middle fs-4'),
                    
            dash_bc.CardBody(
                [
                    html.H5('Session ID: '+str(sessid),className='lh-lg',id='sessid'+str(rig_name)),
                    html.P('Protocols: '+protocol,className='lh-1',id='protocol'+str(rig_name)),
                    html.P(['Subject ID:',dash_bc.Badge("%s"%subjid,color='white',text_color="dark",href=subjid,className='fs-6 fw-normal ms-0')],className='lh-0',id='subjid'+str(rig_name)),
                    html.P('Number of trials: '+str(num_trial),className='lh-1',id='trails'+str(rig_name)),
                    html.P('Total reward: '+str(total_rew)+'ul',className='lh-1',id='torew'+str(rig_name)),
                    html.P('Violation: '+str(viol)+'%',className='lh-1',id='vio'+str(rig_name)),
                    html.P('Total hits: '+str(hits)+'%',className='lh-1',id='hits'+str(hits)),
                    html.P(time_passed_str,className='lh-1',id='time'+str(hits)),
                    dash_bc.Progress(value=round(show_time/80*100),striped=progress_animation,animated=progress_animation,
                                     label="%s"%round(show_time/80*100) + "%",style={"height": "20px"},color="rgb(0,134,149)"),
                    html.Div(id={'type':'button_out','index':str(rig_name)}), #this is just to show if the button works, will delete later
                ]
            ),
        ],
        id = str(rig_name),
        style = {'width':'18rem','padding':'5px'},
        color = rig_color,
        outline = True,
        className = 'border border-3 shadow mb-0 bg-body rounded-3',
        )
    return rig_card

# make the children of all the rigs for auto refresh
def rigs_cards(df,btn_lock):
    rigs_sum = list()
    for i in range(df.shape[0]):
        df_this = df.iloc[[i],:]
        rig_card = generate_rig_card(df_this,btn_lock)
        rigs_sum.append(rig_card)
    return rigs_sum
        
    
FM_rat_rig_cards = html.Div([
    dash_bc.Row(
        id = 'fm_rats_rigs',
        className = "d-grid gap-3 mx-auto d-md-flex",
        ),
    ])

FM_mouse_rig_cards = html.Div([
    dash_bc.Row(
        id = 'fm_mice_rigs',
        className = "d-grid gap-3 mx-auto d-md-flex",
        ),
    ])

HF_mouse_rig_cards = html.Div([
    dash_bc.Row(
        id = 'hf_mice_rigs',
        className = "d-grid gap-3 mx-auto d-md-flex",
        ),
    ])

# start building the tabs
rigtab1_content = dash_bc.Card(
    dash_bc.CardBody(
        [
            html.H4('Free Moving Rat Rigs',className='fs-4'),
            html.P(''),
            html.P(''),
            FM_rat_rig_cards,
            ]
        )
    )

rigtab2_content = dash_bc.Card(
    dash_bc.CardBody(
        [
            html.H4('Free Moving Mouse Rigs',className='fs-4'),
            html.P(''),
            html.P(''),
            FM_mouse_rig_cards,
            ]
        )
    )

rigtab3_content = dash_bc.Card(
    dash_bc.CardBody(
        [
            html.H4('Head Fixed Mouse Rigs',className='fs-4'),
            html.P(''),
            html.P(''),
            HF_mouse_rig_cards,
            ]
        )
    )

rigtabs = dash_bc.Tabs(
    [
     dash_bc.Tab(rigtab1_content,label='Free Moving Rats'),
     dash_bc.Tab(rigtab2_content,label='Free Moving Mice'),
     dash_bc.Tab(rigtab3_content,label='Head Fixed Mice'),
     dcc.Store(id = 'current_rigs_data'),
     dcc.Interval(
         id = 'dash_update_rigs',
         interval = int(refresh_rate),
         n_intervals = 0,
         )
     ],
    active_tab='tab-0',
    id='rig_tabs'
    )

# we add a new page to log the cage water status
def generate_subjid_cardbody(cage_name,cage_water_status,last_status_time,cageid_db,current_animal_cages):
    delta_time_cage = dt.datetime.now()-last_status_time
    days_c, hours_c, minutes_c, seconds_c = delta_time_cage.days, delta_time_cage.seconds // 3600, delta_time_cage.seconds // 60 % 60, delta_time_cage.seconds % 60
    
    cage_water_d = {'waterin':'water in','waterout':'water out'}

    cage_water_cardbody_content = [
        html.P(f'CageID: {cageid_db}',className='lh-1'),
        html.P(f'Time since {cage_water_d[cage_water_status]}: {days_c*24+hours_c}:{minutes_c}:{seconds_c}',className='lh-1'),
        html.Div(id={'type':'cage_btn_out','index':cage_name},),
        ]

    subjids = current_animal_cages[current_animal_cages['pyratcageid']==cage_name]['subjid']
    for i in subjids:
        subjid_badge = html.H5([dash_bc.Badge(i,href=i,color='light',text_color="primary",className='border me-1 shadow',pill=True)])
        cage_water_cardbody_content.append(subjid_badge)
    return cage_water_cardbody_content

def generate_cage_card(df,current_animal_cages,btn_lock):
    cage_name = df.iloc[0,4]
    cage_water_status = df.iloc[0,1]
    last_status_time = pd.to_datetime(df.iloc[0,2])
    cageid_db = df.iloc[0,3]
    cage_cardcontent = generate_subjid_cardbody(cage_name,cage_water_status,last_status_time,cageid_db,current_animal_cages)
    if cage_water_status == 'waterin':
        cage_btn_lb = 'Take out'
        cage_btn_cl = 'warning'
        cage_card_cl = 'primary'
    elif cage_water_status == 'waterout':
        cage_btn_lb = 'Put in'
        cage_btn_cl = 'success'
        cage_card_cl = 'danger'
        
    cage_card = dash_bc.Card(
        [
            dash_bc.CardHeader([cage_name,
                                dash_bc.Button(cage_btn_lb,
                                               color=cage_btn_cl,
                                               id={'type':'cage_btn','index':cage_name},
                                               className='float-end',
                                               n_clicks=0,disabled=(not btn_lock))],
                               className='align-middle fs-4'),
            dash_bc.CardBody(cage_cardcontent),
            ],
        style={'width':'22rem','height':'22rem','padding':'5px'},
        color=cage_card_cl,
        outline=True,
        className='border border-4 shadow mb-0 bg-body rounded-3'
        )
    return cage_card

def cage_cards(df,current_animal_cages,btn_lock):
    cage_sum=list()
    for i in range(df.shape[0]):
        df_this = df.iloc[[i],:]
        cage_card = generate_cage_card(df_this,current_animal_cages,btn_lock)
        cage_sum.append(cage_card)
    return cage_sum

rat_cage_status_cards = html.Div([
    dash_bc.Row(
        id = 'rats_cages_status',
        className = "d-grid gap-3 mx-auto d-md-flex",
        ),
    ])

mice_cage_status_cards = html.Div([
    dash_bc.Row(
        id = 'mice_cages_status',
        className = "d-grid gap-3 mx-auto d-md-flex",
        ),
    ])

# start building the tabs
rats_cage_status_tab1 = dash_bc.Card(
    dash_bc.CardBody(
        [
            html.H4('Rat cages (Make sure you are in the right page!!!)',className='fs-4'),
            html.Br(),
            html.Br(),
            rat_cage_status_cards,
            ]
        )
    )

mice_cage_status_tab2 = dash_bc.Card(
    dash_bc.CardBody(
        [
            html.H4('Mouse cages (Make sure you are in the right page!!!)',className='fs-4'),
            html.Br(),
            html.Br(),
            mice_cage_status_cards,
            ]
        )
    )

cage_water_tabs = dash_bc.Tabs(
    [
     dash_bc.Tab(rats_cage_status_tab1,label='Rat Cages'),
     dash_bc.Tab(mice_cage_status_tab2,label='Mouse Cages'),
     dcc.Store(id = 'current_cages_data'),
     dcc.Interval(
         id = 'dash_update_water_status',
         interval = int(refresh_rate),
         n_intervals=0,
         ),
     ],
    active_tab='tab-0',
    id='cage_tabs'
    )
    


# here make the editable schedule table

schedule_pg = html.Div(
    [
     html.H4 ('Select the date of the schedule to show', className='fs-4'),
     html.Br(),
     html.Div([
     dcc.DatePickerSingle(
         id = 'schedule_date_picker',
         min_date_allowed = date(2022,8,1),
         max_date_allowed = date.today() + dt.timedelta(1),
         date = date.today(),
         display_format = 'YYYY-MM-DD',
         className = 'float-start',
         style={'width':'max-content'},
         ),
     dcc.ConfirmDialogProvider(
         children = dash_bc.Button('Update Schedule',color='primary',className='btn-lg ms-5 float-none',id='botton_update_schedule'),
         id = 'danger_update_schedule',
         message = 'Are you sure you want to update the schedule now?'),
        dash_bc.Button("Download Schedule", id="btn-download-txt", color='primary',className='btn-lg ms-5 float-none'),
        dcc.Download(id="download-text")
     ],
         className = 'align-middle',
         style={"display":"flex"}),
     html.Br(),
     html.Br(),
     html.Div([
              dash_table.DataTable(
                  id = 'editing_schedule',
                  style_table = {'height':'auto'},
                  style_cell = {'textAlign':'center'},
                  style_header = {'fontWeight':'bold','height':'70px'},
                  style_data_conditional=[
                      {
                          'if': {'row_index': 'odd'},
                          'backgroundColor': 'rgb(237, 237, 237)',
                          },
                      {'height':'70px','width':'150px'},
                      ]
                  )],
         id = 'date_schedule'),
     html.Br(),
     html.Hr(className = 'my-2'),
     html.Br(),
     html.P('You can edit the commets and technotes here:',className='fs-4'),
     html.Br(),
     html.Div([
         'Subjid:',
         dcc.Input(id='schedule_inp_subjid',type='text',placeholder='subjid',className='ms-2 me-3',size='10'),
         ' Comments:',
         dcc.Input(id='schedule_inp_comments',type='text',placeholder='any comments',className='ms-2 me-3'),
         ' Technotes:',
         dcc.Input(id='schedule_inp_technotes',type='text',placeholder='any technotes',className='ms-2 me-3'),
         dash_bc.Button('Submit',id='submit_comments',color='primary',outline=True,size='lg',className='ms-3'),
         ]),
     html.Div(id = 'update_schedule'),
     html.Div(id = 'submit_comments_states'),
     ],
    style = {'font-size':'20px'},
    )


# make the sessview table
sessview_content = html.Div(
    [
     html.H4 ('Select the date range for the session view', className='fs-4'),
     html.Br(),
     dcc.DatePickerRange(
         id = 'sessview_date_picker',
         min_date_allowed = date(2022,8,1),
         display_format = 'YYYY-MM-DD',
         #className = 'float-start',
         style={'width':'max-content'},
         ),
     html.Br(),
     html.Br(),
     dash_table.DataTable(
         id='sessview_tb',
         style_table = {'height':'auto'},
         style_cell = {'textAlign':'center'},
         style_header = {'fontWeight':'bold','height':'70px'},
         style_data_conditional=[
             {
                 'if': {'row_index': 'odd'},
                 'backgroundColor': 'rgb(237, 237, 237)',
                 },
             {'height':'50px','width':'150px'},
             ],
         page_current=0,
         page_size=25,
         sort_action='native',
         sort_mode='multi',
         filter_action='native',
         
         )
     ]
    )




# make the current mass view table
current_mass_view_water_controlled_content = html.Div([
    html.Br(),
    html.H4 ("Today's mass view for water controlled animals", className='float-none fs-4'),
    html.P("Download the mass data from the link:", className='float-start fs-6'),
    #dash_bc.Badge("Mass to pyrat",href='http://lab.deneuro.org/mass/today_mass_to_pyrat.csv',external_link=True,color="white",text_color='primary',className='border float-none ms-3'),
    dash_bc.Badge("Mass to Gsheets",href='http://lab.deneuro.org/mass/water_restriction_daily_weight_check.csv',external_link=True,color="white",text_color='primary',className='border float-none ms-3'),
    dash_bc.Button("Export View for PyRat", id="btn-download-table-water-controlled", color="primary",className='border ms-2' , style={"padding":"2px 6px", "font-size":"medium"}),
    dcc.Download(id="download-table-water-controlled") ,
    html.Br(),
    html.Br(),
    dash_table.DataTable(
        id='today_mass_water_controlled_view',
        style_table = {'height':'auto'},
        style_cell = {'textAlign':'center'},
        style_header = {'fontWeight':'bold','height':'70px'},
        style_data_conditional=[
            {
            'if': {'row_index': 'odd'},
            'backgroundColor': 'rgb(237, 237, 237)'},
            {'height':'50px','width':'150px'},
            {
            'if': {'filter_query': '{weight_percentage} < 0.85 && {weight_percentage} > 0.8', 'column_id':'weight_percentage'},
            'backgroundColor': 'tomato',
            'color':'white'},
            {
            'if': {'filter_query': '{weight_percentage} <= 0.8 && {weight_percentage} > 0', 'column_id':'weight_percentage'},
            'backgroundColor': '#85144b',
            'color':'white'},
            {
            'if': {'filter_query': '{mass} >300 && {total_reward}<5000 && {total_reward}>0', 'column_id':'total_reward'},
            'backgroundColor': '#85144b',
            'color':'white'},
            {
            'if': {'filter_query': '{mass} <60 && {total_reward}<400 && {total_reward}>0', 'column_id':'total_reward'},
            'backgroundColor': '#85144b',
            'color':'white'},
            ],
        page_current=0,
        sort_action='native',
        sort_mode='multi',
        filter_action='native',
        # export_format="csv",
        cell_selectable=True
        )
    ])

current_mass_view_water_controlled_mice_content = html.Div([
    html.Br(),
    html.H4 ("Today's mass view for water controlled mice", className='float-none fs-4'),
    dash_bc.Button("Export View for PyRat", id="btn-download-table-water-controlled-mice", color="primary",className='border ms-2' , style={"padding":"2px 6px", "font-size":"medium"}),
    dcc.Download(id="download-table-water-controlled-mice") ,
    html.Br(),
    html.Br(),
    dash_table.DataTable(
        id='today_mass_water_controlled_mice_view',
        style_table = {'height':'auto'},
        style_cell = {'textAlign':'center'},
        style_header = {'fontWeight':'bold','height':'70px'},
        style_data_conditional=[
            {
            'if': {'row_index': 'odd'},
            'backgroundColor': 'rgb(237, 237, 237)'},
            {'height':'50px','width':'150px'},
            {
            'if': {'filter_query': '{weight_percentage} < 0.85 && {weight_percentage} > 0.8', 'column_id':'weight_percentage'},
            'backgroundColor': 'tomato',
            'color':'white'},
            {
            'if': {'filter_query': '{weight_percentage} <= 0.8 && {weight_percentage} > 0', 'column_id':'weight_percentage'},
            'backgroundColor': '#85144b',
            'color':'white'},
            {
            'if': {'filter_query': '{mass} >300 && {total_reward}<5000 && {total_reward}>0', 'column_id':'total_reward'},
            'backgroundColor': '#85144b',
            'color':'white'},
            {
            'if': {'filter_query': '{mass} <60 && {total_reward}<400 && {total_reward}>0', 'column_id':'total_reward'},
            'backgroundColor': '#85144b',
            'color':'white'},
            ],
        page_current=0,
        sort_action='native',
        sort_mode='multi',
        filter_action='native',
        # export_format="csv",
        cell_selectable=True
        )
    ])


current_mass_view_water_controlled_rat_content = html.Div([
    html.Br(),
    html.H4 ("Today's mass view for water controlled mice", className='float-none fs-4'),
    dash_bc.Button("Export View for PyRat", id="btn-download-table-water-controlled-rat", color="primary",className='border ms-2' , style={"padding":"2px 6px", "font-size":"medium"}),
    dcc.Download(id="download-table-water-controlled-rat") ,
    html.Br(),
    html.Br(),
    dash_table.DataTable(
        id='today_mass_water_controlled_rat_view',
        style_table = {'height':'auto'},
        style_cell = {'textAlign':'center'},
        style_header = {'fontWeight':'bold','height':'70px'},
        style_data_conditional=[
            {
            'if': {'row_index': 'odd'},
            'backgroundColor': 'rgb(237, 237, 237)'},
            {'height':'50px','width':'150px'},
            {
            'if': {'filter_query': '{weight_percentage} < 0.85 && {weight_percentage} > 0.8', 'column_id':'weight_percentage'},
            'backgroundColor': 'tomato',
            'color':'white'},
            {
            'if': {'filter_query': '{weight_percentage} <= 0.8 && {weight_percentage} > 0', 'column_id':'weight_percentage'},
            'backgroundColor': '#85144b',
            'color':'white'},
            {
            'if': {'filter_query': '{mass} >300 && {total_reward}<5000 && {total_reward}>0', 'column_id':'total_reward'},
            'backgroundColor': '#85144b',
            'color':'white'},
            {
            'if': {'filter_query': '{mass} <60 && {total_reward}<400 && {total_reward}>0', 'column_id':'total_reward'},
            'backgroundColor': '#85144b',
            'color':'white'},
            ],
        page_current=0,
        sort_action='native',
        sort_mode='multi',
        filter_action='native',
        # export_format="csv",
        cell_selectable=True
        )
    ])


current_mass_water_controlled_tabs = dash_bc.Tabs(
        [
            dash_bc.Tab(current_mass_view_water_controlled_content,label='All'),
            dash_bc.Tab(current_mass_view_water_controlled_mice_content,label='Mice'),
            dash_bc.Tab(current_mass_view_water_controlled_rat_content,label='Rats'),
        ],
        active_tab='tab-0',
        id='current_mass_water_controlled_tabs'
    )



# make the current mass view table
current_mass_view_water_free_content = html.Div([
    html.Br(),
    html.H4 ("Mass view for Free Water animals", className='float-none fs-4'),
    html.Br(),
    html.Br(),
    dash_table.DataTable(
        id='today_mass_water_free_view',
        style_table = {'height':'auto'},
        style_cell = {'textAlign':'center'},
        style_header = {'fontWeight':'bold','height':'70px'},
        style_data_conditional=[
            {'height':'50px','width':'150px'},
            {
                'if': {'row_index': 'odd'},
                'backgroundColor': 'rgb(237, 237, 237)'
            },
            
            {
                'if': {'filter_query': f'{{last_mass_date}} <= {(dt.datetime.today() - dt.timedelta(days = 7)).strftime("%Y-%m-%dT%H:%M:%S")}', 'column_id':'last_mass_date'},
                'backgroundColor': 'tomato',
                'color':'white'
            },
            {
                'if': {'filter_query': f'{{last_mass_date}} <= {(dt.datetime.today() - dt.timedelta(days = 6)).strftime("%Y-%m-%dT%H:%M:%S")}', 'column_id':'last_mass_date'},
                'backgroundColor': '#e3c51b',
                'color':'black'
            },

            
            ],
        page_current=0,
        sort_action='native',
        sort_mode='multi',
        filter_action='native',
        # export_format="csv",
        cell_selectable=True
        )
    ])


current_mass_view_water_free_mice_content = html.Div([
    html.Br(),
    html.H4 ("Mass view for Free Water animals", className='float-none fs-4'),
    html.Br(),
    html.Br(),
    dash_table.DataTable(
        id='today_mass_water_free_mice_view',
        style_table = {'height':'auto'},
        style_cell = {'textAlign':'center'},
        style_header = {'fontWeight':'bold','height':'70px'},
        style_data_conditional=[
            {'height':'50px','width':'150px'},
            {
                'if': {'row_index': 'odd'},
                'backgroundColor': 'rgb(237, 237, 237)'
            },
            
            {
                'if': {'filter_query': f'{{last_mass_date}} <={(dt.datetime.today() - dt.timedelta(days = 7)).strftime("%Y-%m-%dT%H:%M:%S")}',
                        'column_id':'last_mass_date'
                        },
                'backgroundColor': 'tomato',
                'color':'white'
            },
            {
                'if': {'filter_query': f'{{last_mass_date}} <= {(dt.datetime.today() - dt.timedelta(days = 6)).strftime("%Y-%m-%dT%H:%M:%S")}',
                        'column_id':'last_mass_date'
                        },
                'backgroundColor': '#e3c51b',
                'color':'black'
            },

            
            ],
        page_current=0,
        sort_action='native',
        sort_mode='multi',
        filter_action='native',
        # export_format="csv",
        cell_selectable=True
        )
    ])

current_mass_view_water_free_rat_content = html.Div([
    html.Br(),
    html.H4 ("Mass view for Free Water animals", className='float-none fs-4'),
    html.Br(),
    html.Br(),
    dash_table.DataTable(
        id='today_mass_water_free_rat_view',
        style_table = {'height':'auto'},
        style_cell = {'textAlign':'center'},
        style_header = {'fontWeight':'bold','height':'70px'},
        style_data_conditional=[
            {'height':'50px','width':'150px'},
            {
                'if': {'row_index': 'odd'},
                'backgroundColor': 'rgb(237, 237, 237)'
            },
            
            {
                'if': {'filter_query': f'{{last_mass_date}} <= {(dt.datetime.today() - dt.timedelta(days = 7)).strftime("%Y-%m-%dT%H:%M:%S")}', 
                       'column_id':'last_mass_date'
                       },
                'backgroundColor': 'tomato',
                'color':'white'
            },
            {
                'if': {'filter_query': f'{{last_mass_date}} <= {(dt.datetime.today() - dt.timedelta(days = 6)).strftime("%Y-%m-%dT%H:%M:%S")}', 
                       'column_id':'last_mass_date'
                      },
                'backgroundColor': '#e3c51b',
                'color':'black'
            },

            
            ],
        page_current=0,
        sort_action='native',
        sort_mode='multi',
        filter_action='native',
        # export_format="csv",
        cell_selectable=True
        )
    ])



current_mass_water_free_tabs = dash_bc.Tabs(
        [
            dash_bc.Tab(current_mass_view_water_free_content,label='All'),
            dash_bc.Tab(current_mass_view_water_free_mice_content,label='Mice'),
            dash_bc.Tab(current_mass_view_water_free_rat_content,label='Rats'),
        ],
        active_tab='tab-0',
        id='current_mass_water_free_tabs'
    )

#make massview tabs
mass_view_tabs = dash_bc.Tabs(
        [
        dash_bc.Tab(current_mass_water_controlled_tabs,label='Today Water Controlled'),
        dash_bc.Tab(current_mass_water_free_tabs,label='Free Water')
        ],
        active_tab='tab-0',
        id='mass_view_tabs'
    )



# make the Utility contents in the dash

# comments logger
comments_logger = [
    html.Br(),
    dash_bc.CardHeader(['Comments logger'],className = 'fs-3'),
    dash_bc.CardBody([
        html.Br(),
        html.P('Input subjid:',className='fs-4'),
        dcc.Dropdown(id='comments_subjid',searchable=True,style={'width':'370px'},placeholder='XXX-R/M-0000'),
        html.Br(),
        html.P('Input your comments to the subject:', className='fs-4 ms-6'),
        dash_bc.Textarea(id='comments_subj_inpt',size='lg',className='ms-6'),
        html.Br(),
        html.P('Experimenter:', className='fs-4 ms-6'),
        dcc.Dropdown(id='comments_select_exper',searchable=True,className='ms-6'),
        html.Br(),
        dash_bc.Button('Submit',id='comments_subj_btn',color='primary',outline=True,size='lg',className='ms-6'),
        html.Hr(),
        html.Div(id='submit_subject_comments')
        ]),
    
    ]

# hf position updater
hf_position_logger = [
    html.Br(),
    dash_bc.CardHeader(['Update the lick spouts postion'],className = 'fs-3'),
    dash_bc.CardBody([
        html.Br(),
        html.P('Input subjid:',className='fs-4'),
        dcc.Dropdown(id='hf_lickposition_subjid_dropD',searchable=True,style={'width':'370px'},placeholder='XXX-M-0000'),
        html.Br(),
        html.P('Use the slider to update the mouth position (the dot indicate the midline of mouth):', className='fs-4 ms-6'),
        html.Br(),
        html.Div([
            dcc.Slider(-3,3,1,
                       id = 'slider-update_hf_spoutsP',
                       marks={
                           -3:'-3L',
                           -2:'-2',
                           -1:'-1',
                           0:'0',
                           1:'1',
                           2:'2',
                           3:'3R',
                           },
                       included = False),
            ],  style= {'marginLeft':'78px','marginRight':'70px','transform':'scale(1.5)','width': '25%'}),
        
        html.Br(),
        html.P('Experimenter:', className='fs-4 ms-6'),
        dcc.Dropdown(id='hf_spoutsP_select_exper',searchable=True,className='ms-6'),
        html.Br(),
        dash_bc.Button('Submit',id='hf_souptsP_btn',color='primary',outline=True,size='lg',className='ms-6'),
        html.Hr(),
        html.Div(id='submit_hf_subject_position')
        ]),
    ]

# mannually start one session
start_sess = [
     html.Br(),
     dash_bc.CardHeader([
         'Choose a subjid and rigid to start this session:'
         ],
         className = 'fs-3'),
     dash_bc.CardBody([
         html.Div([
             html.P('Select the subjid:',className='fs-4'),
             dcc.Dropdown(id='select_subjid',searchable=True,style={'width':'370px'}),
             html.Br(),
             html.P('Select the rigid:',className='fs-4'),
             dcc.Dropdown(id='select_rigid',searchable=True,style={'width':'370px'}),
             ]),
         html.Br(),
         dcc.ConfirmDialogProvider(
             children = dash_bc.Button('Start', color='success',id='button_start_sess',disabled=False,size='lg'),
             id='danger_start_sess',
             message='Are you sure you want to start this session manually?'
             ),
         html.Br(),
         html.Hr(className = 'my-2'),
         html.Div(id='start_sess_manually')
         ])
     ]

# update the animal exp stage
expgroup_stage = [
    html.Br(),
    dash_bc.CardHeader(
        [
            'Assign or update animal expgroup and training stage'
            ], className='fs-3'),
    dcc.Store(id='current_expr_tb'),
    dash_bc.CardBody(
        [html.P('Input the experid:',className='fs-4'),
         dcc.Dropdown(id='expgroup_stage_select_exper',searchable=True,style={'width':'370px'}),
         html.Br(),
         html.P('Input subjid:',className='fs-4'),
         dash_bc.Input(id='expgroup_stage_input_subjid',style={'width':'370px'},placeholder='XXX-R/M-0000',maxlength=10,minlength=10),
         html.Br(),
         html.P('Input expgroup id: (check the table below, if you are changing stage, ignore this)',className='fs-4'),
         dash_bc.Input(id='expgroup_stage_input_expgroup',style={'width':'370px'},type='number',min=1,max=999,step=1),
         html.Br(),
         html.P('Input stage id: (if you are changing expgroup, ignore this)',className='fs-4'),
         dash_bc.Input(id='expgroup_stage_input_stage',style={'width':'370px'},type='number',min=1,max=999,step=1),
         html.Br(),
         dash_bc.Checkbox(id = 'expgroup_stage_remove_settings',label = 'remove previous settings',value = False),
         html.Br(),
         dash_bc.Button('Update expgroup', color='secondary',id='button_update_expgroup',disabled=False,size='lg'),
         dash_bc.Button('Update stage',color='info',id='button_update_expstage',disabled=False,className='ms-5',size='lg'),
         html.Div(id='alert_update_expgroup_stage'),
         html.Div(id='alert_update_expgroup_stage1')
            ]),
    html.Hr(className='my-2'),
    dash_table.DataTable(id='expgroup_stage_tb',
                         style_header = {'fontWeight':'bold'},
                         style_data_conditional=[{
                                 'if': {'row_index': 'odd'},
                                 'backgroundColor': 'rgb(237, 237, 237)',
                                 }
                             ],
                         page_current=0,
                         page_size=25,
                         style_cell = {'textAlign':'center'}),
    ]

utility_content = dash_bc.Accordion(
    [
     dash_bc.AccordionItem(
         comments_logger, title = 'Make comments to one subject: ',
         ),
     dash_bc.AccordionItem(
         hf_position_logger, title = 'Update hf animal lick position: ',
         ),
     dash_bc.AccordionItem(
         start_sess, title = 'Manually start this session: ',
         ),
     dash_bc.AccordionItem(
         expgroup_stage, title= 'Update expgroup and training stage')
     ],
    #always_open=True
    )

def real_subj_table(pseudo_subjid):
    real_subj_table = html.Div([
        html.Br(),
        html.H3(["Subjid:",dash_bc.Badge("%s"%pseudo_subjid,color='white',href="%s"%pseudo_subjid,text_color="dark",className='fs-3 ms-0')], className='ms-6'),
        html.Hr(),
        dfun.make_pseudo_subjinfo_tb(pseudo_subjid),
        html.Br(),
        html.Hr(),
        ])
    return real_subj_table

# make this function to generate webpage for each animal using subjid

def make_subjid_info_table(subjid):
    subjid_info=html.Div([
        html.Br(),
        html.H3(["Subjid:",dash_bc.Badge("%s"%subjid,color='white',href="beh/%s"%subjid,text_color="dark",className='fs-3 ms-0')], className='ms-6'),
        html.Hr(),
        dfun.make_subjinfo_tb(subjid),
        html.Br(),
        html.Hr(),
        ])
    return subjid_info

def make_subjid_massplot(subjid):
    subjid_massplot = html.Div([
        html.Br(),
        html.H6('Animal mass:', className='fs-5 ms-6'),
        html.Br(),
        dash_bc.Card([
            dash_bc.CardBody([dfun.make_mass_plot(subjid),]),
            ],
            outline = True,
            className = 'border border-3 shadow bg-body rounded-3',),
        html.Hr(),
        ])
    return subjid_massplot

def make_comments_tb(subjid):
    comments_df = dfun.get_subject_comments(subjid)
    comments_tb=html.Div([
        html.Br(),
        html.H6('Comments:', className='fs-5 ms-6'),
        html.Br(),
        dash_table.DataTable(
            style_table = {'height':'auto'},
            style_cell = {'textAlign':'center','height':'auto','maxWidth':0,'whiteSpace': 'normal'},
            style_header = {'fontWeight':'bold'},
            data = comments_df.to_dict('records'),
            columns = [{"name": i, "id": i} for i in comments_df.columns]
            ),
        html.Hr()
        ])
    return comments_tb

def make_current_settings_tb(subjid):
    current_settings_df = dfun.get_subject_current_settings(subjid)
    current_settings_tb=html.Div([
            html.Br(),
            html.H6('Current_settings:', className='fs-5 ms-6'),
            html.Br(),
            html.P("No settings in the database for this subject!",className='fs-6 ms-6')
            ])
    if len(current_settings_df):
        if current_settings_df['settings_data'] is not None and len(current_settings_df['settings_data'])>0 and current_settings_df['settings_data'][0] is not None:
            df=json.loads(current_settings_df['settings_data'][0])
            temp_setting_tb = pd.DataFrame(df)['vals'].rename('Values')
            temp_setting_value = pd.DataFrame(temp_setting_tb)
            temp_setting_name = pd.DataFrame(temp_setting_value.index.rename('Settings_name'))
            settings_df = pd.concat([temp_setting_name.reset_index(drop=True),temp_setting_value.reset_index(drop=True)],axis=1)
            for i in range(0,len(settings_df)):
                settings_df['Values'].iloc[i] = str(settings_df['Values'].iloc[i])
            current_settings_tb=html.Div([
                html.Br(),
                html.H6('Current_settings:', className='fs-5 ms-6'),
                html.Br(),
                dash_table.DataTable(
                    style_table = {'height':'auto'},
                    style_cell = {'textAlign':'center','height':'auto','maxWidth':0,'whiteSpace': 'normal'},
                    style_header = {'fontWeight':'bold'},
                    data = settings_df.to_dict('records'),
                    columns = [{"name": i, "id": i} for i in settings_df.columns]
                    )
                ])
        
    return current_settings_tb

# we can generate behavior plots here
def make_general_beh_plots(subjid):
    general_beh_df = dfun.get_general_beh_data(subjid)
    fig1 = px.scatter(general_beh_df, 
                      x="sessiondate",
                      y="total_profit",
                      color="end_stage", 
                      height=500,
                      labels={"sessiondate": "Session Date","total_profit": "Total Water Reward (µL)"},
                      category_orders= {"end_stage": map(str, general_beh_df.end_stage.unique())})               
    fig1.update_traces(marker=dict(size=8,line=dict(width=.8,color='DarkSlateGrey')))
    fig1.update_layout(legend_title="End Stage",font=dict(size=16),legend_font=dict(size=12))

    fig2 = make_subplots(specs=[[{"secondary_y": True}]])
    fig2_1 = px.line(general_beh_df, 
                   x="sessiondate", 
                   #y=["num_trials","total_hits","violations"],
                   y="num_trials",
                   height=500)
    fig2_1.update_traces(line=dict(width=3,color='rgb(102,102,102)',dash='dot'))
    fig2_2 = px.line(general_beh_df,
                     x='sessiondate',
                     y=["hits","viols"],
                     color_discrete_map={
                         "hits":"#1C8356",
                         "viols":"rgb(253,180,98)"
                         })
    fig2_2.update_traces(yaxis="y2")
    fig2.add_traces(fig2_1.data + fig2_2.data)
    fig2.layout.xaxis.title="Session Date"
    fig2.layout.yaxis.title="Total trials"
    fig2.layout.yaxis2.title="Value (%)"
    fig2.update_layout(
        font=dict(size=16),
        yaxis=dict(showgrid=False,title_font_color='rgb(102,102,102)',tickfont_color='rgb(102,102,102)'),
        yaxis2=dict(title_font_color='#1C8356',tickfont_color='#1C8356')
        )
    '''
    newnames = {"num_trials": "Num. Trials", 
                    "total_hits": "Hits", 
                    "violations": "Viols"}
    fig2.for_each_trace(lambda t: t.update(
        name = newnames[t.name],
        legendgroup = newnames[t.name],
        hovertemplate = t.hovertemplate.replace(t.name,newnames[t.name])))
    '''
    general_beh_plots = html.Div([
        dash_bc.Card([
            dash_bc.CardHeader([html.P('Water Reward Gained Each Session')],className = 'align-middle fs-4'),
            dash_bc.CardBody([dcc.Graph(figure = fig1)])
            ],
            style = {'margin-bottom':'25px'},
            outline = True,
            className = 'border border-3 shadow mb-3 bg-body rounded-3',),
        dash_bc.Card([
            dash_bc.CardHeader([html.P('Number of Trials, Hits and Viols')],className = 'align-middle fs-4'),
            dash_bc.CardBody([dcc.Graph(figure = fig2)])
            ],
            outline = True,
            className = 'border border-3 shadow mb-3 bg-body rounded-3',),
        ])
    return general_beh_plots

def generate_subjid_session_plots(subjid):
    protocols = dfun.check_subjid_current_protocol(subjid)
    if len(protocols) == 0:
        return html.Div([
            html.Br(),
            html.H1("NO DATA!",className = 'text-danger'),
            html.Br(),
            html.H3('The current animal has no session data yet!',className='fs-3 ms-6'),
            html.Hr(),
            ])
    else:
        if protocols.iloc[0,0] == "RiskyChoice_fm":
            return html.Div([
                html.Br(),
                html.H1("Psychometrics for %s" %protocols.iloc[0,0],className = 'text-primary'),
                html.Br(),
                html.P("Psychometrics plots for subjid: %s" %subjid,className='fs-4 ms-6'),
                html.Hr(),
                html.Br(),
                dash_bc.Row(dfun.generate_psychometric_cards(subjid),className = "d-grid gap-3 mx-auto d-md-flex")
                ])
        else:
            return html.Div([
                html.Br(),
                html.H1("NO CODE TO GENERATE PLOTS!",className = 'text-info'),
                html.Br(),
                html.H3('You can write code to generate plots for %s!' %protocols.iloc[0,0],className='fs-3 ms-6'),
                html.Hr(),
                ])


def general_hehavior_plots(subjid):
    current_protocol = dfun.check_subjid_current_protocol(subjid)
    if len(current_protocol)>0:
        behavior_plots = make_general_beh_plots(subjid)
    else:
        behavior_plots = html.Div([
            html.P('No session data in the database for this subject!',className='fs-6 ms-6'),
            ])
    
    behavior_part=html.Div([
        html.Br(),
        html.H6('Behavior plots:', className='fs-5 ms-6'),
        html.Br(),
        behavior_plots,
        ])
    return behavior_part

def generate_pseudo_subjid_page(pseudo_subjid):
    webpage_subjid = html.Div([
        real_subj_table(pseudo_subjid),
        html.Br()
        ])
    return webpage_subjid

def generate_subjid_page(subjid):
    webpage_subjid = html.Div([
        make_subjid_info_table(subjid),
        html.Br(),
        dash_bc.Row(
            [dash_bc.Col([make_comments_tb(subjid),make_current_settings_tb(subjid)],width=6),
             dash_bc.Col([make_subjid_massplot(subjid),general_hehavior_plots(subjid)],width=6)],
            className="align-items-md-stretch"),
        html.Br(),
        ])
    return webpage_subjid

# navbar
navbar = dash_bc.NavbarSimple(
    children=[
        DashIconify(icon='akar-icons:lock-on',width=30,className='me-3 mt-1',color='white',id='switch_enable_icon'),
        daq.BooleanSwitch(on=False,id='switch_enable_btn',className='dark-theme-control mt-2 me-5'),
        DashIconify(icon='iconoir:sidebar-collapse',width=40,className='me-3',color='white',id='btn_sidebar_icon'),
        dash_bc.Button("Sidebar", outline=True, color="secondary", className="mr-1", id="btn_sidebar"),
    ],
    brand="DELab Dashboard",
    brand_href="#",
    color="dark",
    dark=True,
    fluid=True,
    sticky='top'
)

# side bar
SIDEBAR_STYLE = {
    "transition": "all 1s",
    "position": "fixed",
    "top": 62.5,
    "left": 0,
    "bottom": 0,
    "width": "14rem",
    "padding": "0.5rem 1rem",
    "background-color": "#f8f9fa",
    "z-index":1,
    "overflow-x": "hidden",
}

CONTENT_STYLE = {
    "transition":"margin-left .5s",
    "margin-left": "15rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}

SIDEBAR_HIDEN = {
    "transition": "all 1s",
    "position": "fixed",
    "top": 62.5,
    "left": '-13rem',
    "bottom": 0,
    "width": "13rem",
    "padding": "0.5rem 1rem",
    "background-color": "#f8f9fa",
    "z-index":1,
    "overflow-x": "hidden",
}


CONTENT_STYLE1 = {
    "transition":"margin-left .5s",
    "margin-left": "2rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}


sidebar = html.Div(
    [
        html.Br(),
        html.Hr(className = 'my-5'),
        html.P(id='db_connection_empty', className="lead"
        ),
        dcc.Interval(
         id = 'checking_db_connections',
         interval = int(refresh_rate),
         n_intervals = 0,
         ),
        dash_bc.Nav(
            [
                dash_bc.NavLink([DashIconify(icon='ant-design:right-square-outlined',width=30,className='me-3'),"Rigs"], href="/", active="exact"),
                dash_bc.NavLink([DashIconify(icon='mdi:water-alert-outline',width=30,className='me-3'),"Water Status"], href="/water", active="exact"),
                dash_bc.NavLink([DashIconify(icon='carbon:event-schedule',width=30,className='me-3'),"Schedule"], href="/schedule", active="exact"),
                dash_bc.NavLink([DashIconify(icon='carbon:table-shortcut',width=30,className='me-3'),"Session View"], href="/sessview", active="exact"),
                dash_bc.NavLink([DashIconify(icon='ion:scale-outline',width=30,className='me-3'),"Mass View"], href="/massview", active="exact"),
                dash_bc.NavLink([DashIconify(icon='carbon:tools-alt',width=30,className='me-3'),"Utility"], href="/utility", active="exact"),
            ],
            vertical=True,
            pills=True,
        ),
    ],
    style=SIDEBAR_STYLE,
    id='sidebar-content'
)

content = html.Div(id="page-content", style=CONTENT_STYLE)

click_event = {"event": "click"}
keydown_event= {"event": "keydown"}

app.layout = html.Div([dcc.Store(id='sidebar-btn-click'),
                       #dcc.Store(id='store-switch-enable-btn'),
                       dcc.Store(id='current_animal_incages'),
                       dcc.Store(id='page_for_subjid'),
                       dcc.Location(id="url"),
                       navbar,
                       sidebar,
                       content
    ])


@app.callback(
    Output('db_connection_empty','title'),
    Input('checking_db_connections','n_intervals')
    )
def check_db_connection(interval):
    if interval % 300 == 1:
        try:
            dfun.DBC.use('met')
            check_DBE = pd.read_sql('select 1',dfun.DBE)
            return ""
        except:
            dfun.db_reconnect()
            return ""

@app.callback(
    Output('current_rigs_data','data'),
    Input('dash_update_rigs','n_intervals')
    )
def fetch_rig_data(interval):
    try:
        rig_stat_file_ = HOME + '/.deneuro/conf/rig_status.csv'
        if read_from_ceph:
            rig_stat_file_ = 'http://lab.deneuro.org/conf/rig_status.csv'
        new_rigs = pd.read_csv(rig_stat_file_,index_col=0)
        current_rigs_tb = new_rigs.to_json(orient='split')
        return current_rigs_tb
    except:
        pass


@app.callback(
    Output('fm_rats_rigs','children'),
    Output('fm_mice_rigs','children'),
    Output('hf_mice_rigs','children'),
    Input('dash_update_rigs','n_intervals'),
    State('current_rigs_data','data'),
    State('switch_enable_btn','on'),
    )
def update_rig_cards(ts,datasets,btn_lock):
    if datasets is None:
        raise PreventUpdate
    else:
        new_rigs = pd.read_json(datasets, orient='split')
        FM_rat_rigs = dfun.FM_rat_rig(new_rigs)
        rats_rig_cards = rigs_cards(FM_rat_rigs,btn_lock)
        FM_mice_rigs = dfun.FM_mouse_rig(new_rigs)
        mice_rig_cards = rigs_cards(FM_mice_rigs,btn_lock)
        HF_mice_rigs = dfun.HF_mouse_rig(new_rigs)
        hf_mice_rig_cards = rigs_cards(HF_mice_rigs,btn_lock)
        
        return rats_rig_cards,mice_rig_cards,hf_mice_rig_cards

@app.callback(
    Output({'type':'button_out','index': MATCH},'children'),
    Input({'type':'rig_button','index': MATCH},'n_clicks'),
    State({'type':'rig_button','index': MATCH},'ids'),
    State('current_rigs_data','data')
    )
def rig_button_reaction(n_clicks, ids,current_rigs_tb):
    ctx = dash.callback_context
    if (ctx.triggered_id != None) & (current_rigs_tb != None):
        if current_rigs_tb is None:
            raise PreventUpdate
        else:
            new_rigs = pd.read_json(current_rigs_tb,orient='split')
            
            button_rigid = int(ctx.triggered_id['index'])
            button_rig_df = new_rigs[new_rigs['rigid'] == button_rigid]
            #button_subjid = button_rig_df.iloc[0,9]
            button_sessid = button_rig_df.iloc[0,7]
            rig_status = dfun.get_rig_status(button_rig_df)
            if rig_status == 'running':
                MSG.stop_session(button_sessid,button_rigid)
            elif rig_status == 'testing':
                MSG.stop_rigtest(button_rigid)
            elif rig_status == 'stopped':
                MSG.rig_test(button_rigid)
    return []

@app.callback(
    Output('current_cages_data','data'),
    Input('dash_update_water_status','n_intervals')
    )
def fetch_cage_data(interval):
    try:
        current_cage_water_file_ = HOME + '/.deneuro/conf/current_cage_water.csv'
        if read_from_ceph:
            current_cage_water_file_ = 'http://lab.deneuro.org/conf/current_cage_water.csv'
        new_cages = pd.read_csv(current_cage_water_file_,index_col=0)
        current_cages_tb = new_cages.to_json(orient='split')
        return current_cages_tb
    except:
        pass

@app.callback(
    Output('rats_cages_status','children'),
    Output('mice_cages_status','children'),
    Input('dash_update_water_status','n_intervals'),
    State('current_cages_data','data'),
    State('current_animal_incages','data'),
    State('switch_enable_btn','on'),
    )
def update_cage_cards(ts,datasets,animal_incages_data,btn_lock):
    if datasets is None:
        raise PreventUpdate
    else:
        all_cages = pd.read_json(datasets, orient='split')
        current_animal_incages = pd.read_json(animal_incages_data, orient='split')
        current_cages = all_cages.query("cageid in {}".format(list(set(current_animal_incages.loc[:,'cageid']))))
        rats_cages_df = current_cages[current_cages.iloc[:,3]//100%2==1]
        rats_cage_cards = cage_cards(rats_cages_df,current_animal_incages,btn_lock)
        mice_cages_df = current_cages[current_cages.iloc[:,3]//100%2==0]
        mice_cage_cards = cage_cards(mice_cages_df,current_animal_incages,btn_lock)
        return rats_cage_cards,mice_cage_cards

@app.callback(
    Output({'type':'cage_btn_out','index': MATCH},'children'),
    Input({'type':'cage_btn','index': MATCH},'n_clicks'),
    State({'type':'cage_btn','index': MATCH},'ids'),
    State('current_cages_data','data')
    )
def cage_btn_reaction(nclicks,ids,current_cages_tb):
    ctx=dash.callback_context
    if ctx.triggered_id != None:
        new_cages = pd.read_json(current_cages_tb,orient='split')
        
        cageid = ctx.triggered_id['index']
        btn_cage_df = new_cages[new_cages['pyratcageid']==cageid]
        cageid_db = btn_cage_df.iloc[0,3]
        cage_status = btn_cage_df.iloc[0,1]
        if cage_status == 'waterin':
            cage_status_now = 'waterout'
        elif cage_status == 'waterout':
            cage_status_now = 'waterin'
        now_time = dt.datetime.now()
        now_time_str = now_time.strftime('%Y-%m-%d %H:%M:%S')
        experid = 0
        dfun.change_water_status(cage_status_now, now_time_str, cageid_db, experid)
        #print(ctx.triggered_id['index'])

# update date picker time on page refresh
@app.callback(
    Output('schedule_date_picker','max_date_allowed'),
    Output('schedule_date_picker','date'),
    Input('url','pathname')
    )
def update_schedule_datepicker(pathname):
    max_date_allowed = date.today() + dt.timedelta(1)
    now_date = date.today()
    return max_date_allowed,now_date

@app.callback(
    Output('editing_schedule','data'),
    Output('editing_schedule','columns'),
    Output('editing_schedule','tooltip_data'),
    Input('schedule_date_picker','date')
    )
def schedule_table(date_value):
    if date_value is not None:
        
        schedule_data = dfun.make_schedule_data(date_value,'subjid')
        comments_data = dfun.make_schedule_data(date_value, 'comments')
        #print(schedule_data)
        data = schedule_data.to_dict('records')
        columns = [{'id':c,'name':c,'editable':(c!='rigid')} for c in schedule_data.columns]
        tooltip_data = [{
            column:{'value':str(value) if value is not None else '', 'type':'markdown'}
            for column,value in row.items()}
            for row in comments_data.to_dict('records')]
        return data,columns,tooltip_data
    
@app.callback(
    Output('schedule_inp_subjid','value'),
    Output('schedule_inp_comments','value'),
    Output('schedule_inp_technotes','value'),
    Input('editing_schedule','active_cell'),
    State('editing_schedule','data'),
    State('schedule_date_picker','date'))
def active_cell_info(active_cell,new_schedule,picked_date):
    if active_cell is not None:
        now_schedule = pd.DataFrame(new_schedule)
        prev_comments = dfun.make_schedule_data(picked_date,'comments')
        prev_technotes = dfun.make_schedule_data(picked_date,'technotes')
        # print(prev_comments)
        # print(prev_comments.loc[active_cell['row'],active_cell['column_id']])
        if now_schedule.loc[active_cell['row'],active_cell['column_id']] is None:
            subjid = ''
        else:
            subjid = now_schedule.loc[active_cell['row'],active_cell['column_id']]
            
        if prev_comments.loc[active_cell['row'],active_cell['column_id']] is None:
            comments = ''
        else:
            comments = prev_comments.loc[active_cell['row'],active_cell['column_id']]
            
        if prev_technotes.loc[active_cell['row'],active_cell['column_id']] is None:
            technotes = ''
        else:
            technotes = prev_technotes.loc[active_cell['row'],active_cell['column_id']]
    else:
        subjid=''
        comments=''
        technotes=''
    return subjid,comments,technotes


@app.callback(
    Output("download-text", "data"),
    Input("btn-download-txt", "n_clicks"),
    prevent_initial_call=True,
)
def make_download_schedule(n_clicks):
    # make the pretty pdf here
    # get the animal schedules 

    pdf_file = dfun.get_schedule_file()


    return dcc.send_file(pdf_file)

    
@app.callback(
    Output('update_schedule','children'),
    Input('danger_update_schedule','submit_n_clicks'),
    State('editing_schedule','data'),
    State('schedule_date_picker','date')
    )
def update_schedule_b(n_clicks,data_editing,picked_date):
    if n_clicks != None:
        prev_schedule = dfun.make_schedule_data(picked_date,'subjid')
        prev_comments = dfun.make_schedule_data(picked_date,'comments')
        prev_technotes = dfun.make_schedule_data(picked_date,'technotes')
        now_schedule = pd.DataFrame(data_editing)
        prev_schedule1 = prev_schedule.fillna(0)
        now_schedule1 = now_schedule.fillna(0)
        diff_schedule = (now_schedule1 != prev_schedule1)
        if sum(sum(diff_schedule.values)):
            for index, rows in diff_schedule.iteritems():
                for i in range(len(rows)):
                    if rows[i]:
                        ins_rigid = now_schedule1.iloc[i,0]
                        ins_date = picked_date
                        ins_time = index
                        ins_subjid = now_schedule1.loc[i,index]
                        ins_comments = prev_comments.loc[i,index]
                        ins_technotes = prev_technotes.loc[i,index]
                        if ins_comments is None:
                            ins_comments = ''
                        if ins_technotes is None:
                            ins_technotes = ''
                        
                        dfun.change_schedule_item(ins_rigid,ins_date,ins_time,ins_subjid,ins_comments,ins_technotes)
                        #dfun.insert_schedule_data(ins_rigid,ins_date,ins_time,ins_subjid)                   
    return []

@app.callback(
    Output('submit_comments_states','children'),
    Input('submit_comments','n_clicks'),
    State('schedule_inp_subjid','value'),
    State('schedule_inp_comments','value'),
    State('schedule_inp_technotes','value'),
    State('editing_schedule','data'),
    State('schedule_date_picker','date'))
def submit_comments_b(n_clicks,subjid,comments,technotes,new_schedule,picked_date):
    if n_clicks != None:
        now_schedule = pd.DataFrame(new_schedule)
        bool_schedule = now_schedule == subjid
        if sum(sum(bool_schedule.values)):
            for index,rows in bool_schedule.iteritems():
                for i in range(len(rows)):
                    if rows[i]:
                        ins_rigid = now_schedule.iloc[i,0]
                        ins_date = picked_date
                        ins_time = index
                        dfun.change_schedule_item(ins_rigid,ins_date,ins_time,subjid,comments,technotes)
                        return [dash_bc.Alert('Comments or technotes updated',color='success',duration=2000)]
                        
        else:
            return [dash_bc.Alert('make sure you have a input in subjid',color='danger',duration=2000)]

# update date picker time on page refresh
@app.callback(
    Output('sessview_date_picker','max_date_allowed'),
    Output('sessview_date_picker','start_date'),
    Output('sessview_date_picker','end_date'),
    Input('url','pathname')
    )
def update_sessview_daterange(pathname):
    now_date = date.today()
    return now_date,now_date+dt.timedelta(-6),now_date
    
# show sessview based on date range
@app.callback(
    Output('sessview_tb','data'),
    Output('sessview_tb','columns'),
    Input('sessview_date_picker','start_date'),
    Input('sessview_date_picker','end_date')
    )
def update_sessview_tb(start_date,end_date):
    sessview_df=dfun.get_sessview_daterange(start_date, end_date)
    sessview_df['subjid'] = list(map(str.__add__,"["+sessview_df['subjid']+"](/",sessview_df['subjid']+")"))
    data = sessview_df.to_dict('records')
    columns = [{'id':c,'name':c,'presentation':'markdown'} if c == 'subjid' else {'id':c,'name':c} for c in sessview_df.columns]
    return data,columns

# create today's massview
@app.callback(
    Output('today_mass_water_controlled_view','data'),
    Output('today_mass_water_controlled_view','columns'),
    Input('url','pathname')
    )
def update_mass_water_controlled_tb(pathname):
    current_mass_tb = dfun.get_current_water_controled_tb()
    current_mass_tb['subjid'] = list(map(str.__add__,"["+current_mass_tb['subjid']+"](/",current_mass_tb['subjid']+")"))
    data = current_mass_tb.to_dict('records')
    columns = [{'id':c,'name':c,'presentation':'markdown'} if c == 'subjid' else {'id':c,'name':c} for c in current_mass_tb.columns]
    return data,columns

#get the current data from the DataTable and print on the terminal
@app.callback(
    Output('download-table-water-controlled','data'),
    Input("btn-download-table-water-controlled", "n_clicks"),
    State('today_mass_water_controlled_view','derived_virtual_data'),
    prevent_initial_call=True,
)
def print_current_mass_data(n_clicks,table_data):
    df = pd.DataFrame.from_dict(table_data)
    df = df[['pyratid', 'mass_date', 'mass']]
    #change date format
    df['mass_date'] = pd.to_datetime(df['mass_date']).dt.strftime('%Y-%m-%d')
    df = df.rename(columns={'pyratid':'Identifier','mass_date':'Date','mass':'Weight'})
    
    #make a partial function to convert the dataframe to csv without index 
    csv_string =  df.to_csv(index=False)

    return dcc.send_string(csv_string, "pyrat.csv")



# create today's massview for water controlled rats
@app.callback(
    Output('today_mass_water_controlled_rat_view','data'),
    Output('today_mass_water_controlled_rat_view','columns'),
    Input('url','pathname')
    )
def update_today_mass_water_controlled_rat_tb(pathname):
    current_mass_tb = dfun.get_current_water_controled_tb()
    #filter all the rat only. subjid have the format ...-R-....
    current_mass_tb = current_mass_tb[current_mass_tb['subjid'].str[3:6]=='-R-']
    current_mass_tb['subjid'] = list(map(str.__add__,"["+current_mass_tb['subjid']+"](/",current_mass_tb['subjid']+")"))
    data = current_mass_tb.to_dict('records')
    columns = [{'id':c,'name':c,'presentation':'markdown'} if c == 'subjid' else {'id':c,'name':c} for c in current_mass_tb.columns]
    return data,columns

#get the current data from the DataTable and download it
@app.callback(
    Output('download-table-water-controlled-rat','data'),
    Input("btn-download-table-water-controlled-rat", "n_clicks"),
    State('today_mass_water_controlled_rat_view','derived_virtual_data'),
    prevent_initial_call=True,
)
def print_current_mass_data(n_clicks,table_data):
    df = pd.DataFrame.from_dict(table_data)
    df = df[['pyratid', 'mass_date', 'mass']]
    #change date format
    df['mass_date'] = pd.to_datetime(df['mass_date']).dt.strftime('%Y-%m-%d')
    df = df.rename(columns={'pyratid':'Identifier','mass_date':'Date','mass':'Weight'})
    
    #make a partial function to convert the dataframe to csv without index 
    csv_string =  df.to_csv(index=False)

    return dcc.send_string(csv_string, "pyrat.csv")




# create today's massview for water controlled mice
@app.callback(
    Output('today_mass_water_controlled_mice_view','data'),
    Output('today_mass_water_controlled_mice_view','columns'),
    Input('url','pathname')
    )
def update_mass_water_controlled_mice_tb(pathname):
    current_mass_tb = dfun.get_current_water_controled_tb()
    #filter all the rat only. subjid have the format ...-R-....
    current_mass_tb = current_mass_tb[current_mass_tb['subjid'].str[3:6]=='-M-']
    current_mass_tb['subjid'] = list(map(str.__add__,"["+current_mass_tb['subjid']+"](/",current_mass_tb['subjid']+")"))
    data = current_mass_tb.to_dict('records')
    columns = [{'id':c,'name':c,'presentation':'markdown'} if c == 'subjid' else {'id':c,'name':c} for c in current_mass_tb.columns]
    return data,columns

#get the current data from the DataTable and download it for PyRat
@app.callback(
    Output('download-table-water-controlled-mice','data'),
    Input("btn-download-table-water-controlled-mice", "n_clicks"),
    State('today_mass_water_controlled_mice_view','derived_virtual_data'),
    prevent_initial_call=True,
)
def print_current_mass_data(n_clicks,table_data):
    df = pd.DataFrame.from_dict(table_data)
    df = df[['pyratid', 'mass_date', 'mass']]
    #change date format
    df['mass_date'] = pd.to_datetime(df['mass_date']).dt.strftime('%Y-%m-%d')
    df = df.rename(columns={'pyratid':'Identifier','mass_date':'Date','mass':'Weight'})
    
    #make a partial function to convert the dataframe to csv without index 
    csv_string =  df.to_csv(index=False)

    return dcc.send_string(csv_string, "pyrat.csv")




# create massview for water free
@app.callback(
    Output('today_mass_water_free_view','data'),
    Output('today_mass_water_free_view','columns'),
    Input('url','pathname')
    )
def update_mass_water_free_tb(pathname):
    current_mass_tb = dfun.get_free_water_animal_tb()
    current_mass_tb['subjid'] = list(map(str.__add__,"["+current_mass_tb['subjid']+"](/",current_mass_tb['subjid']+")"))
    data = current_mass_tb.to_dict('records')
    columns = [{'id':c,'name':c,'presentation':'markdown'} if c == 'subjid' else {'id':c,'name':c} for c in current_mass_tb.columns]
    return data,columns

# create massview for water free rats
@app.callback(
    Output('today_mass_water_free_rat_view','data'),
    Output('today_mass_water_free_rat_view','columns'),
    Input('url','pathname')
    )
def update_mass_water_free_rat_tb(pathname):
    current_mass_tb = dfun.get_free_water_animal_tb()
    #filter all the rat only. subjid have the format ...-R-....
    current_mass_tb = current_mass_tb[current_mass_tb['subjid'].str[3:6]=='-R-']
    current_mass_tb['subjid'] = list(map(str.__add__,"["+current_mass_tb['subjid']+"](/",current_mass_tb['subjid']+")"))
    data = current_mass_tb.to_dict('records')
    columns = [{'id':c,'name':c,'presentation':'markdown'} if c == 'subjid' else {'id':c,'name':c} for c in current_mass_tb.columns]
    return data,columns

# create massview for water free mice

@app.callback(
    Output('today_mass_water_free_mice_view','data'),
    Output('today_mass_water_free_mice_view','columns'),
    Input('url','pathname')
    )
def update_mass_water_free_mice_tb(pathname):
    current_mass_tb = dfun.get_free_water_animal_tb()
    #filter all the rat only. subjid have the format ...-R-....
    current_mass_tb = current_mass_tb[current_mass_tb['subjid'].str[3:6]=='-M-']
    current_mass_tb['subjid'] = list(map(str.__add__,"["+current_mass_tb['subjid']+"](/",current_mass_tb['subjid']+")"))
    data = current_mass_tb.to_dict('records')
    columns = [{'id':c,'name':c,'presentation':'markdown'} if c == 'subjid' else {'id':c,'name':c} for c in current_mass_tb.columns]
    return data,columns

@app.callback(
    Output('select_subjid','options'),
    Output('select_rigid','options'),
    Input('url','pathname')
    )
def prepare_start_session_data(pathname):
    # since the call back on the url, put all the url options here
    #print(pathname)
    today_date = dt.datetime.now().strftime('%Y-%m-%d')
    today_schedule = dfun.make_schedule_data(today_date,'subjid')
    today_rigs = list(filter(None,today_schedule.iloc[:,0].to_numpy().flatten()))
    today_subjid = list(filter(None,today_schedule.iloc[:,1:5].to_numpy().flatten()))
    return today_subjid,today_rigs

# call back for manully start one session
@app.callback(
    Output('start_sess_manually','children'),
    Input('danger_start_sess','submit_n_clicks'),
    State('select_subjid','value'),
    State('select_rigid','value')
    )
def start_session_manually(n_clicks,subjid,rigid):
    if n_clicks is not None:
        if (subjid != '') and (rigid != ''):
            message = 'subjid %s in rig %s should be started' %(subjid,str(rigid))
            
            MSG.start_session(subjid, rigid)
            return [dash_bc.Alert(message,color='success',duration=2000)]
        else:
            message = 'Please make sure you have both subjid and rigid selected!!!'
            return [dash_bc.Alert(message,color='danger',duration=2000)]
        
@app.callback(
    Output('expgroup_stage_select_exper','options'),
    Output('expgroup_stage_tb','data'),
    Output('expgroup_stage_tb','columns'),
    Output('current_expr_tb','data'),
    Output('comments_select_exper','options'),
    Output('comments_subjid','options'),
    Output('hf_lickposition_subjid_dropD','options'),
    Output('hf_spoutsP_select_exper','options'),
    Input('url','pathname'),
    )
def fetch_expgroup_stage(pathname):
    exper = dfun.get_experid()
    all_subjids = dfun.get_all_subjectids()
    mice_subjids = all_subjids[all_subjids['subjid'].str.contains('-M-')]
    exper_tb = exper.to_json(orient='split')
    tb = dfun.get_expgroup_stage()
    data = tb.to_dict('records')
    columns = [{'id':c,'name':c} for c in tb.columns]
    return exper.iloc[:,0].tolist(),data,columns,exper_tb,exper.iloc[:,0].tolist(),all_subjids.iloc[:,0].tolist(),mice_subjids.iloc[:,0].tolist(),exper.iloc[:,0].tolist()

@app.callback(
    Output('alert_update_expgroup_stage','children'),
    Input('button_update_expgroup','n_clicks'),
    State('expgroup_stage_select_exper','value'),
    State('expgroup_stage_input_subjid','value'),
    State('expgroup_stage_input_expgroup','value'),
    State('current_expr_tb','data'),
    )
def btn_update_expgroup(n_clicks,exper,subjid,expgroup,exper_tb):
    if n_clicks is not None:
        if exper is None:
            return [html.Br(),dash_bc.Alert('You need select your exper name!!!',color='danger',duration=2000)]
        else:
            if subjid is None:
                return [html.Br(),dash_bc.Alert('You need input correct subjid!!!',color='danger',duration=2000)]
            else:
                if expgroup is None:
                    return [html.Br(),dash_bc.Alert('Search a correct expgroup below!!!',color='danger',duration=2000)]
                else:
                    exper_df = pd.read_json(exper_tb, orient='split')
                    experid = int(exper_df[exper_df['Firstname']==exper].iloc[0,1])
                    dfun.assign_expgroup(subjid,expgroupid=expgroup,experid=experid)
                    return [html.Br(),dash_bc.Alert('successfully updated',color='success',duration=2000)]

# Call back for comments logger                
@app.callback(
    Output('submit_subject_comments','children'),
    Input('comments_subj_btn','n_clicks'),
    State('comments_subjid','value'),
    State('comments_subj_inpt','value'),
    State('comments_select_exper','value'),
    State('current_expr_tb','data')
    )
def submit_comment_subjid(nclick,subjid,comments,exper_name,exper_tb):
    if nclick is not None:
        if (subjid is None) or (comments is None) or (exper_name is None) or (comments == ""):
            return [html.Br(),dash_bc.Alert('Need input all above information!!!',color='danger',duration=2000)]
        else:
            exper_df = pd.read_json(exper_tb, orient='split')
            experid = int(exper_df[exper_df['Firstname']==exper_name].iloc[0,1])
            dfun.submit_comments_subjid(subjid,comments,experid)
            return [html.Br(),dash_bc.Alert('Comments upload successfully',color='success',duration=2000)]
        
# call back for hf spouts position logger
@app.callback(
    Output('slider-update_hf_spoutsP','value'),
    Input('hf_lickposition_subjid_dropD','value'),
    )
def get_hf_lick_position(subjid):
    if subjid is not None:
        subj_lick_position = dfun.get_lick_position_subjid(subjid)
        return subj_lick_position
    else:
        subj_lick_position = 0
        return subj_lick_position
    
@app.callback(
    Output('submit_hf_subject_position','children'),
    Input('hf_souptsP_btn','n_clicks'),
    State('hf_lickposition_subjid_dropD','value'),
    State('slider-update_hf_spoutsP','value'),
    State('hf_spoutsP_select_exper','value'),
    State('current_expr_tb','data')
    )
def update_hf_subject_lickP(nclick,subjid,lick_position,exper_name,exper_tb):
    if nclick is not None:
        if (subjid is None) or (exper_name is None):
            return [html.Br(),dash_bc.Alert('Need input all above information!!!',color='danger',duration=2000)]
        else:
            exper_df = pd.read_json(exper_tb, orient='split')
            experid = int(exper_df[exper_df['Firstname']==exper_name].iloc[0,1])
            dfun.update_hf_lick_position(subjid,lick_position,experid)
            return [html.Br(),dash_bc.Alert('Lick position upload successfully',color='success',duration=2000)]
    


'''
@app.callback(
    Output('comments_select_exper','options'),
    Input('url','pathname'),
    State('current_expr_tb','data'),
    )
def send_comments_exper(pathname,exper_tb):
    print(exper_tb)
    exper_df = pd.read_json(exper_tb, orient='split')
    print(exper_df)
    return exper_df.iloc[:,0].tolist()
'''
@app.callback(
    Output('alert_update_expgroup_stage1','children'),
    Input('button_update_expstage','n_clicks'),
    State('expgroup_stage_select_exper','value'),
    State('expgroup_stage_input_subjid','value'),
    State('expgroup_stage_input_stage','value'),
    State('current_expr_tb','data'),
    State('expgroup_stage_remove_settings','value')
    )
def btn_update_expstage(n_clicks,exper,subjid,expstage,exper_tb,remove_settings_checked):
    if n_clicks is not None:
        if exper is None:
            return [html.Br(),dash_bc.Alert('You need to select your exper name!!!',color='danger',duration=5000)]
        else:
            if subjid is None:
                return [html.Br(),dash_bc.Alert('You need to input correct subjid!!!',color='danger',duration=5000)]
            else:
                if expstage is None:
                    return [html.Br(),dash_bc.Alert('Search a correct expgroup below!!!',color='danger',duration=5000)]
                else:
                    exper_df = pd.read_json(exper_tb, orient='split')
                    experid = exper_df[exper_df['Firstname']==exper].iloc[0,1]
                    dfun.set_subject_stage(subjid,stage=expstage,experid=int(experid),clear_subject_settings=remove_settings_checked)
                    return [html.Br(),dash_bc.Alert('successfully updated',color='success',duration=1000)]
        
# check the button lock switch to enable or disable the buttons
# @app.callback(
#     Output('switch_enable_icon','icon'),
#     Output('store-switch-enable-btn','data'),
#     Input('switch_enable_btn','on')
#     )
# def change_button_enable_icon(btn_lock):
#     if btn_lock:
#         lock_icon='akar-icons:lock-off'
#         store_switch_btn = False
#         return lock_icon,store_switch_btn
#     else:
#         lock_icon='akar-icons:lock-on'
#         store_switch_btn = True
#         return lock_icon,store_switch_btn
    
# @app.callback(
#     Output({'type':'rig_button','index': ALL},'disabled'),
#     Output({'type':'cage_btn','index': ALL},'disabled'),
#     Input('store-switch-enable-btn','data'),
#     State({'type':'rig_button','index': ALL},'n_clicks'),
#     State({'type':'cage_btn','index': ALL},'n_clicks')
#     )
# def change_button_enable(data,rig_btn_dic,cage_btn_dic):
#     return [data]*len(rig_btn_dic),[data]*len(cage_btn_dic)

@app.callback(
    Output('switch_enable_icon','icon'),
    Input('switch_enable_btn','on'),
    )
def change_button_enable_status(btn_lock):
    if btn_lock:
        lock_icon='akar-icons:lock-off'
        return lock_icon
    else:
        lock_icon='akar-icons:lock-on'
        return lock_icon
    lock_icon='akar-icons:lock-on'
    return lock_icon

@app.callback(
    Output('sidebar-content','style'),
    Output('page-content','style'),
    Output('sidebar-btn-click','data'),
    Output('btn_sidebar_icon','icon'),
    Input('btn_sidebar','n_clicks'),
    State('sidebar-btn-click','data')
    )
def toggle_sidebar(nclicks,data):
    if nclicks:
        if data == 'show':
            sidebar_style = SIDEBAR_HIDEN
            content_style = CONTENT_STYLE1
            cur_data = 'hidden'
            icon = 'iconoir:sidebar-expand'
        else:
            sidebar_style = SIDEBAR_STYLE
            content_style = CONTENT_STYLE
            cur_data = 'show'
            icon = 'iconoir:sidebar-collapse'
    else:
        sidebar_style = SIDEBAR_STYLE
        content_style = CONTENT_STYLE
        cur_data = 'show'
        icon = 'iconoir:sidebar-collapse'
        
    return sidebar_style,content_style,cur_data,icon        


@app.callback(
    Output("page-content", "children"),
    Output('current_animal_incages','data'),
    [Input("url", "pathname")]
    )
def render_page_content(pathname):
    # here get the current animal cage when page refresh
    animal_cages = dfun.get_current_animal_cage(read_from_ceph)
    current_animal_incages = animal_cages.to_json(orient='split')
    if pathname == "/":
        return html.Div(
            [rigtabs]
            ),current_animal_incages
    elif pathname == "/water":
        return html.Div(
            [cage_water_tabs]
            ),current_animal_incages
    elif pathname == "/schedule":
        return html.Div(
            [schedule_pg]
            ),current_animal_incages
    elif pathname == "/sessview":
        return html.Div(
            [sessview_content]
            ),current_animal_incages
    elif pathname == '/massview':
        return html.Div(
            [mass_view_tabs]
            ),current_animal_incages
    elif pathname == "/utility":
        return html.Div(
            [utility_content]
            ),current_animal_incages
    elif pathname == "/session_expired":
        # create a simple session expired page
        # with a simple text show Session Expired
        return html.Div(
            [
                html.H1("Session Expired", className="text-danger"),
                html.Hr(),
                html.H4("Please click on the tabs in the side menu to continue...",className='text-info'),
            ]
        ),None
    elif re.search('/[A-Z]{3}\-[M|R]\-\d{4}',pathname):
        if len(pathname)==11:
            subjid = pathname[1:11]
            if dfun.get_subject_info(subjid).shape[0]:
                return html.Div(
                    [generate_subjid_page(subjid)]
                    ),current_animal_incages
            else:
                return dash_bc.Container(
                    [
                        html.H1("Animal not in the DB", className="text-danger"),
                        html.Hr(),
                        html.H4(f"The subjid '{subjid}' was not in the database, try another one ...",className='text-info'),
                    ],fluid = True,className='py-3',
                ),current_animal_incages
        elif pathname[0:4]=='/beh':
            subjid = pathname[5:15]
            if dfun.get_subject_info(subjid).shape[0]:
                return html.Div(
                    [generate_subjid_session_plots(subjid)]
                    ),current_animal_incages
            else:
                return dash_bc.Container(
                    [
                        html.H1("Animal not in the DB", className="text-danger"),
                        html.Hr(),
                        html.H4(f"The subjid '{subjid}' was not in the database, try another one ...",className='text-info'),
                    ],fluid = True,className='py-3',
                ),current_animal_incages
    elif re.search('/[A-Z]{3}\-[P]\-\d{4}',pathname):
        subjid = pathname[1:11] if len(pathname)==11 else pathname[5:15]
        # get the real subject for the pseudo subjects 
        return html.Div(
            [generate_pseudo_subjid_page(subjid)]
            ),current_animal_incages
    


    else:
    # If the user tries to reach a different page, return a 404 message
        return dash_bc.Container(
            [
                html.H1("404: Not found", className="text-danger"),
                html.Hr(),
                html.H4(f"The pathname '{pathname}' was not recognised...",className='text-info'),
            ],fluid = True,className='py-3',
        ),current_animal_incages


if __name__ == "__main__":
    #app.run_server(host='0.0.0.0',debug=False)
    app.run_server(debug=True,port=8888)
    #http_server = WSGIServer(('',5000), app)
    #http_server.serve_forever()
