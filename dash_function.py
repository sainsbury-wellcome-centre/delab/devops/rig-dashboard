#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
from sqlalchemy import text
import json
from datetime import datetime
from helpers import zmqhelper,net
import helpers.DBUtilsClass as db
from datetime import date
import sys
import os
import numpy as np
import plotly
import plotly.graph_objects as go
import dash_bootstrap_components as dash_bc
from dash import html, dcc, dash_table
from plotly.subplots import make_subplots
import plotly.express as px
import warnings
import statsmodels.api as sm
import statsmodels
from scipy.special import expit
from sklearn.linear_model import LogisticRegression
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

matplotlib.use('agg')

DBC = db.Connection()
DBE = db.Engine()
DBC.use('met')


HOME = os.path.expanduser("~")

def db_reconnect():
    global DBC,DBE
    try:
        del DBC,DBE
        DBC = db.Connection()
        DBE = db.Engine()
        DBC.use('met')
        print(datetime.now())
        print('successfully reconnected')
    except:
        DBC = db.Connection()
        DBE = db.Engine()
        DBC.use('met')
        print(datetime.now())
        print('lost DBC,DBE, successfully reconnected!')


def readDB_SQL(sqlQuery: str) -> pd.DataFrame:
    df = pd.DataFrame()
    try:
        with DBE.begin() as connection: 
            df = pd.read_sql(
                sql=text(sqlQuery),
                con=connection,
            )
    except:
        raise Exception("Problem reading SQL Query using Pandas")
    return df


def rigList():
    sqlstr = 'select rigid, status, isbroken, istesting, autorun, last_contact from met.rig_view order by rigid'
    df = DBC.query(sqlstr)
    df = pd.DataFrame(df)
    return df

def FM_rat_rig(df):
    fm_rat_rigL = df[(df.iloc[:,0]>172000) & (df.iloc[:,0]<300000)]
    return fm_rat_rigL

def FM_mouse_rig(df):
    fm_mouse_rigL = df[((df.iloc[:,0]>373100) & (df.iloc[:,0]<373120)) | ((df.iloc[:,0]>464000) & (df.iloc[:,0]<464100))]
    return fm_mouse_rigL

def HF_mouse_rig(df):
    hf_mouse_rigL = df[(df.iloc[:,0]>370000) & (df.iloc[:,0]<373100)]
    return hf_mouse_rigL

def get_rig_status(df):
    if df.iloc[0,1] == 'ready':
        if df.iloc[0,3] == 1:
            rig_current_status = 'testing'
        else:
            rig_current_status = 'stopped'
    elif df.iloc[0,1] is None:
        rig_current_status = 'running'
    else:
        rig_current_status = df.iloc[0,1]
        
    return rig_current_status

def insert_schedule_data(rigid,datestr,timestr,subjid):
    sqlstr = f"""
        insert into met.schedule (rigid,slotdate,slottime,subjid) values
        ({rigid}, '{datestr}','{timestr}', '{subjid}') on duplicate
        key update subjid = '{subjid}; 
        """
    DBC.execute(sqlstr)
    

def get_current_animal_cage(read_from_ceph=False):
    
    
    current_animal_cage = None    
    try:
        if read_from_ceph:
             current_animal_cage = pd.read_csv('http://lab.deneuro.org/conf//current_animal_cage.csv')
        else:
            current_animal_cage = pd.read_csv(HOME + '/.deneuro/conf/current_animal_cage.csv')
    except:
        # read from db if no local file
        sqlstr='select * from met.current_animal_cage where cageid is not NULL'
        current_animal_cage = readDB_SQL(sqlstr)
    return current_animal_cage

def change_water_status(cage_status_now, now_time_str, cageid_db, experid):
    DBC.use('met')
    sqlstr="insert into cage_water_status (status, ts, cageid, experid) values ('%s', '%s', %d, %d)" %(cage_status_now, now_time_str, cageid_db, experid)
    #print(sqlstr)
    DBC.execute(sqlstr)
    
def get_sessview_daterange(start_date, end_date):
    sqlstr="select sessid,subjid,protocol,stage,sess_min,sessiondate,num_trials,total_profit,hits,viols,owner,mass from beh.sessview "\
        "where sessiondate >= '%s' and sessiondate <= '%s' order by sessid DESC" %(start_date, end_date)
    sessview_tb =readDB_SQL(sqlstr)
    return sessview_tb

def get_schedule(slotdate,slottime):
    date_str = slotdate.strftime('%Y-%m-%d')
    sqlstr = f"""
        select a.rigid, b.slotdate, b.slottime, c.subjid, c.cageid, c.owner, 
        d.protocol,d.settingsname,b.comments, b.technotes from met.rig_view as a 
        left join met.schedule as b on a.rigid = b.rigid and b.slotdate = '{date_str}' and b.slottime = '{slottime}'
        left join met.animals as c using (subjid) left join met.current_settings 
        as d using (subjid) where a.autorun=1
        """
    df = readDB_SQL(sqlstr)
    df = pd.DataFrame(df)
    return df

def make_schedule_data(picked_date, picked_columns):
    date_object = date.fromisoformat(picked_date)
    column_name = ['rigid','slotdate','slottime','subjid','cageid','onwer',
                   'protocol','settingsname','comments','technotes']
    sch_df1 = get_schedule(date_object, '10:00:00')
    sch_df2 = get_schedule(date_object, '11:30:00')
    sch_df3 = get_schedule(date_object, '13:00:00')
    sch_df4 = get_schedule(date_object, '14:30:00')
    sch_df5 = get_schedule(date_object, '16:00:00')
    sch_df1.columns = column_name
    sch_df2.columns = column_name
    sch_df3.columns = column_name
    sch_df4.columns = column_name
    sch_df5.columns = column_name
    
    schedule_data = pd.concat([sch_df1[['rigid']],sch_df1[[picked_columns]],sch_df2[[picked_columns]],
                               sch_df3[[picked_columns]],sch_df4[[picked_columns]],sch_df5[[picked_columns]]],axis = 1)
    schedule_data.columns = ['rigid','10:00','11:30','13:00','14:30','16:00']
    return schedule_data

# change schedule base on rigid, date, time, subjid, comments, technotes
def change_schedule_item(rigid, dateslot, timeslot, subjid, comments, technotes):
    DBC.use('met')
    sqlstr = f"""
        call changeSchedule({rigid},'{dateslot}','{timeslot}','{subjid}','{comments}','{technotes}')
        """

    DBC.execute(sqlstr)
    
def get_experid():
    sqlstr='select Firstname,experid from met.experimenters where tech = 1'
    df = readDB_SQL(sqlstr)
    return df
    
def get_expgroup_stage():
    sqlstr='select expgroupid,stage,settingsname,protocol from met.settings order by expgroupid,stage'
    df = readDB_SQL(sqlstr)
    return df

def get_subject_status(subjid):
	out = DBC.query('select status from animals where subjid = %s',(subjid,))
	try:
		return out[0][0]
	except IndexError:
		print('Subject {} does not exist'.format(subjid))
		return ''
    
def get_subject_expgroup(subjid):
	out = DBC.query('select expgroupid, expgroup from animals where subjid = %s',
						 (subjid, ))
	try:
		return out[0]
	except IndexError:
		return(0, 'Subject {} either does not exist or has not been assigned to an experimental group'.format(subjid))

def notify(msg):
	title = (f"Database Update")
	slack_data = {
        "username": "NotificationBot",
        "attachments": [
            {
                "color": "#9733EE",
                "fields": [
                    {
                        "title": title,
                        "value": msg,
                        "short": "false",
                    }
                ]
            }
        ]
    }
	byte_length = str(sys.getsizeof(slack_data))
	headers = {'Content-Type': "application/json", 'Content-Length': byte_length}
	try:
		net.sendslack(data = json.dumps(slack_data), headers = headers)
	except Exception:
		print('Failed to send slack')
	print(msg)

def set_subject_stage(subjlist, stage=1, experid=0, clear_subject_settings=False):
	'''
	set_subject_stage(subjlist, stage=1):
	Input:
	subjlist        a list of subjids.
	stage           a stage (e.g. from the setting table) to set them to.
	experinfo       the id or name of an experimenter who is making the change
	clear_subject_settings  [False] Normally, the most recent subj_data will be copied to the new stage. setting this to True will clear settings.
	You might use this to jump back to an earlier stage or to skip ahead.
	'''

	

	if not isinstance(subjlist,list):
		subjlist = [subjlist]

	for subjid in subjlist:
		# Get the current settings, including the customized subject data.
		sqlstr = 'select subjid, subj_data, protocol, settingsname, stage from current_settings where subjid = %s'
		current_settings = DBC.query(sqlstr, (subjid, ), as_dict=True)
		# Get the settingid for the new stage, based on the experimental group of the subject
		sqlstr = 'select a.settingsid, protocol, settingsname, stage from settings a, animals b where b.subjid=%s and a.expgroupid=b.expgroupid and a.stage=%s'
		newsetting = DBC.query(sqlstr, (subjid, stage), as_dict=True)

		if newsetting is None or len(newsetting) == 0:
			print('It seems that {} has not yet been assigned to an experimental group. Please do so before running this function'.format(subjid))
			return

		newdict = {'subjid': subjid,
					 'settingsid': newsetting['settingsid'],
					 'saved_on_ip': net.getIP()}
        
		if experid != 0:
			newdict['saved_by_experid'] = experid

		if len(current_settings) > 0: # This is an old animal
			if clear_subject_settings:
				comment = 'and data was cleared.'
			else:
				comment = 'and subj_data was copied.'
				newdict['settings_data'] = current_settings['subj_data']
                
		DBC.saveToDB('subject_settings', newdict)
		if current_settings == {}:
			oldstr = ('initializing stage setting for subjid:'+str(subjid)+' ')
			comment = ''
		else:
			oldstr = 'Subject {subjid} moved from {protocol} stage {stage} ({settingsname}) to '.format(**current_settings)
		newstr = '{protocol} stage {stage} ({settingsname}) '.format(**newsetting)
		notify(oldstr + newstr + comment)


def assign_expgroup(subjlist, expgroupid=None,experid=0):
	'''
		assign_expgroup(subjlist, expgroupid=None)
		Input:
		subjlist    a list of subjids.
		expgroupid  an id from the expgroups table.  If this is not passed in
				we ask for input.
	'''
	if not isinstance(subjlist,list):
		subjlist = [subjlist]

	for subjid in subjlist:
		# Is the animal alive?
		current_status = get_subject_status(subjid)
		if current_status == "dead":
			print("{} is dead. Please double check your subjid".format(subjid))
		else:
			if current_status != "running":
				print("Warning: {} is not currently set to running.".format(subjid))

			current_expgroupid = get_subject_expgroup(subjid)[0]

			if current_expgroupid == 0: # No current group
				DBC.execute('insert into animal_expgroup_status (subjid, expgroupid) values (%s, %s)', (subjid, expgroupid))
				notify("{} assigned to group {}".format(subjid, expgroupid))
				set_subject_stage(subjid, experid)
			else:
				DBC.execute('insert into animal_expgroup_status (subjid, expgroupid) values (%s, %s)',
					(subjid, expgroupid))
				notify("{} moved from group {} to group {}. Remeber to set subject stage.".format(subjid, current_expgroupid, expgroupid))

def get_subject_info(subjid):
    sqlstr="select pyratid, species, strain, gender,NVL(TIMESTAMPDIFF(DAY ,DOB, Date(NOW())),-1) as 'Age(days)', pyratcageid, cageid, water, status, expgroup,owner from met.animals where subjid = '%s'"%subjid
    subject_info = readDB_SQL(sqlstr)
    return subject_info

def get_subject_mass(subjid):
    sqlstr="select subjid, mass_date, mass from met.massview where subjid = '%s' order by mass_date DESC" %subjid
    subject_mass = readDB_SQL(sqlstr)
    return subject_mass

def get_subject_comments(subjid):
    sqlstr="select b.Firstname as Experimenter, a.comment as Comments, a.ts as Time from met.subject_comment as a left join met.experimenters as b using (experid)"\
        "where a.subjid = '%s' order by a.ts DESC" %subjid
    subject_comments = readDB_SQL(sqlstr)
    return subject_comments

def get_subject_current_settings(subjid):
    sqlstr=f"select a.* from met.subject_settings a join met.current_settings using (subjsettingsid) where a.subjid = '{subjid}'";
    current_settings = readDB_SQL(sqlstr)
    return current_settings

def get_all_subjectids():
    sqlstr = 'select unique(subjid) from met.massview'
    all_subjids = readDB_SQL(sqlstr)
    return all_subjids

def submit_comments_subjid(subjid,comment,experid):
    now_time = datetime.now()
    current_ts = now_time.strftime('%Y-%m-%d_%H:%M:%S')
    DBC.use('met')
    DBC.execute('insert into subject_comment (subjid, comment, ts, experid) value (%s, %s, %s, %s)', (subjid,comment,current_ts,experid))
    
def get_current_water_controled_tb():
    sqlstr = 'select * from met.today_mass_summary order by subjid'
    current_water_control_tb = readDB_SQL(sqlstr)
    return current_water_control_tb

def get_free_water_animal_tb():
    sqlstr = """
        select 
            b.subjid,
            b.pyratid,
            b.pyratcageid,
            b.owner,
            a.mdate last_mass_date,
            a.mass
        from 
            met.latest_mass a 
                right join 
            met.animals b 
        on (a.subjid = b.subjid )
        where 
            b.water = "free" 
                and 
            b.status = "free" 
                and 
            b.species != "tester"
        order by last_mass_date desc
    """
    free_water_animal_tb = readDB_SQL(sqlstr)
    return free_water_animal_tb

def check_subjid_current_protocol(subjid):
    sqlstr = "select protocol from beh.sessview where subjid = '%s' order by start_time DESC limit 1" %subjid
    current_protocol = readDB_SQL(sqlstr)
    return current_protocol

def make_pseudo_subjinfo_tb(pseudo_subjid):
    # subject info table

    # get real subject list
    subjid_list = get_real_animal_list(pseudo_subjid)
    subject_info_df = pd.DataFrame()

    subject_info_df['subjid'] = [ f'[{subjid}](/{subjid})' for subjid in subjid_list]
    
    info_tb = dash_table.DataTable(
        style_table = {'height':'auto'},
        style_cell = {'textAlign':'center'},
        style_header = {'fontWeight':'bold','height':'70px'},
        data = subject_info_df.to_dict('records'),
        columns = [{'id':c,'name':c,'presentation':'markdown'} if c == 'subjid' else {'id':c,'name':c} for c in subject_info_df.columns]
        )
    return info_tb

def make_subjinfo_tb(subjid):
    # subject info table
    subject_info_df = get_subject_info(subjid)
    
    info_tb = dash_table.DataTable(
        style_table = {'height':'auto'},
        style_cell = {'textAlign':'center'},
        style_header = {'fontWeight':'bold','height':'70px'},
        data = subject_info_df.to_dict('records'),
        columns = [{"name": i, "id": i} for i in subject_info_df.columns]
        )
    return info_tb

def make_mass_plot(subjid):
    # make the mass plot
    subject_mass_df = get_subject_mass(subjid)
    if len(subject_mass_df)==0:
        mass_plot = html.Div([
            html.P("there is no mass data in the DB for this subject")
            ])
    else:
        subject_mass_df.index = pd.RangeIndex(start=0, stop=len(subject_mass_df), step=1)
        if len(subject_mass_df) == 1:
            subject_mass_df.loc[0,'change'] = 0
        else:
            for i in range(1, len(subject_mass_df)):
                subject_mass_df.loc[i-1, 'change'] = (subject_mass_df.loc[i-1, 'mass'] - subject_mass_df.loc[i, 'mass'])*100/subject_mass_df.loc[i, 'mass']
        mass_fig = make_subplots(specs=[[{"secondary_y": True}]])
        mass_fig1 = px.scatter(subject_mass_df, x='mass_date',y='mass',opacity=0.7)
        mass_fig1.update_traces(marker=dict(size=8,line=dict(width=2,color='DarkSlateGrey')))
        mass_fig2 = px.line(subject_mass_df, x='mass_date',y='change')
        mass_fig2.update_traces(yaxis="y2",line=dict(width=2,color='#FB0D0D',dash='dot'))
        mass_fig.add_traces(mass_fig1.data + mass_fig2.data)
        mass_fig.layout.xaxis.title="Date"
        mass_fig.layout.yaxis.title="Daily Mass (g)"
        mass_fig.layout.yaxis2.title="Mass Change (%)"
        mass_fig.update_layout(
            #xaxis=dict(showgrid=False),
            font=dict(size=16),
            yaxis=dict(showgrid=False,title_font_color='blue',tickfont_color='blue'),
            yaxis2=dict(title_font_color='#FB0D0D',tickfont_color='#FB0D0D'),
            height=500,
            )
        
        mass_plot = dcc.Graph(
            figure = mass_fig,
            )
    return mass_plot

def get_general_beh_data(subjid):
    sqlstr = f"""
                select 
                    num_trials,
                    total_profit,
                    hits,viols,
                    sessiondate,
                    end_stage,
                    protocol,
                    settings_name 
                from 
                    beh.sessview 
                where 
                    subjid = '{subjid}' 
                and 
                    sess_min >= 10 
                ORDER BY sessid desc"""
    df = readDB_SQL(sqlstr)
    df['end_stage'] = list(map(str.__add__,df['protocol'].str.slice(0,4).astype(str)+'--',df['settings_name'].str.slice(0,8).astype(str)))
    return df

# make a funciont to generate psychometric plots
def get_risky_sessids(subjid):
    sqlstr = "select sessid from beh.sessview where subjid = '%s' and protocol = 'RiskyChoice_fm' order by start_time desc limit 20" %subjid
    df = readDB_SQL(sqlstr)
    return df

def preprocess_risky(sessid):
    sqlstr = f"select * from prt.riskychoice_fm_view where sessid = {sessid}"
    df = readDB_SQL(sqlstr)
    surebet = df[df.subj_choice == "surebet"]
    surebet = surebet.drop_duplicates(subset=["sessid", "reward"])
    surebet["total_rew_multi"] = surebet["reward"]/surebet["sb_mag"]
    surebet = surebet[["sessid", "total_rew_multi"]]
    df1 = pd.merge(df, surebet, how="right")
    df1["delta_ev"] = (df1["lottery_mag"]*df1["lottery_prob"] - df1["sb_mag"])*df1["total_rew_multi"]
    df1["forced"] = np.where(df1.lottery_poke.isin(["AnyL", "AnyR"]) & df1.surebet_poke.isin(["AnyL", "AnyR"]), 0, 1)
    df1 = df1.assign(choice=df1.subj_choice.map({'surebet': 0, 'lottery': 1}))
    df1 = df1[df1["RT_choice"] < 5]
    df1['sessiondate'] = df1['trialtime'].apply(lambda x: datetime.date(x))
    df1 = df1[(df1['viol'] == 0) & (df1['forced']==0)]
    return df1

def generate_psycho_figure(df):
    choice_pro = df.groupby('delta_ev')['choice'].value_counts(normalize=True).unstack(fill_value=0).stack().to_frame(name='prob').reset_index()
    lottery_pro = choice_pro[choice_pro['choice']==1]
    if lottery_pro.empty:
        lottery_pro = choice_pro[choice_pro['choice']==0]
        lottery_pro['choice'].replace(to_replace= 0.0, value = 1.0, inplace=True)
        lottery_pro['prob'].replace(to_replace= 1.0, value = 0.0, inplace=True )
    unique_choice = len(df['choice'].unique())
    
    fig = go.Figure()
    if unique_choice>1:
        y = df['choice'].to_numpy()
        x = df['delta_ev'].to_numpy()
        pred_x = np.arange(min(x)*1.2, max(x)*1.2,2)
        warnings.filterwarnings('ignore',category=RuntimeWarning)
        try:
            m = sm.GLM(y.T, np.column_stack([np.ones(x.shape[0]), x]), family=sm.families.Binomial()).fit()
        except statsmodels.tools.sm_exceptions.PerfectSeparationError:
            y = y.reshape(-1,1)
            x = x.reshape(-1,1)
            lr = LogisticRegression()
            m = lr.fit(x,np.ravel(y.astype(int)))
            pred_y = expit(pred_x * lr.coef_ + lr.intercept_).ravel()
        else:
            pred_summary = m.get_prediction(np.column_stack([np.ones(pred_x.shape[0]), pred_x])).summary_frame(alpha=0.1)
            pred_y = pred_summary["mean"]         
            fig.add_trace(go.Scatter(x=pred_x,y=pred_summary["mean_ci_lower"],fill=None,mode='lines',line_color='rgb(184, 247, 212)'))
            fig.add_trace(go.Scatter(x=pred_x,y=pred_summary["mean_ci_upper"],fill='tonexty',mode='lines', line_color='rgb(184, 247, 212)'))
        finally:
            fig.add_trace(go.Scatter(x=pred_x,y=pred_y,name='psychometrics',marker_color='rgb(27,158,119)',hovertemplate='Δ Expected Value: %{x}, Lottery Probability: %{y:.2f}',mode='lines'))
            
    fig.add_trace(go.Scatter(x=lottery_pro["delta_ev"],y=lottery_pro["prob"],mode='markers', marker=dict(color='rgb(188,128,189)',size=9),name='data point',hovertemplate='Δ Expected Value: %{x}, Lottery Probability: %{y:.2f}'))
    fig.add_hline(y=0.5, line_width=1.3, line_dash='dot', line_color='grey')
    fig.add_vline(x=0, line_width=1.3, line_dash='dot', line_color='grey')
    fig.update_xaxes(title='ΔExpected Value',title_standoff=0.2)
    fig.update_yaxes(title="Lottery Probability (%)",title_standoff=0.2)
    fig.update_layout(margin=dict(l=40,r=0, t=0,b=40),font_size=11,showlegend=False,yaxis_range=[-0.03,1.03])
    fig.add_annotation(x=0, y=0.9, text='Trials num. = %s'%len(df),showarrow=False,font=dict(size=16))
    return fig
    
def generate_psychometric_cards(subjid):
    psychometric_cards = list()
    sessids = get_risky_sessids(subjid)
    for sessid in sessids.sessid:
        session_df = preprocess_risky(sessid)
        if len(session_df)>0:
            session_fig = generate_psycho_figure(session_df)
            this_card = dash_bc.Card([
                dash_bc.CardHeader([html.P(f'Session ID: {sessid}',className='float-start ms-1 fs-5'),html.P('Date: %s' %session_df['sessiondate'].iloc[0],className='float-end me-1 fs-5')]),
                dash_bc.CardBody([dcc.Graph(figure = session_fig)]),
                ],
                outline = True,
                style = {'width':'28rem','padding':'5px'},
                className = 'border border-3 shadow mb-3 bg-body rounded-3',)
            psychometric_cards.append(this_card)
    return psychometric_cards

def _draw_as_table(df, pagesize):
    alternating_colors = [['white'] * len(df.columns), ['lightgray'] * len(df.columns)] * len(df)
    alternating_colors = alternating_colors[:len(df)]
    fig, ax = plt.subplots(figsize=pagesize)
    ax.axis('tight')
    ax.axis('off')
    ax.set_title(f'Schedule for {datetime.now().strftime("%d-%m-%Y")}')
    the_table = ax.table(cellText=df.values,
                        colLabels=df.columns,
                        cellLoc='center',
                        colColours=['lightblue']*len(df.columns),
                        loc='upper center',
                        colWidths=[0.045,0.05,0.085,0.1,0.27,0.18,0.18])
    the_table.auto_set_font_size(False)
    the_table.set_fontsize(7)
    the_table.scale(1.5,1.5)
    # the_table.auto_set_column_width(col = list(range(len(df.columns))))
    return fig
  

def dataframe_to_pdf(df, filename, rows_per_page = 20, pagesize=(11, 8.5)):
  with PdfPages(filename) as pdf:
    cols_per_page = len(df.columns) 
    nh = (len(df) // rows_per_page) + (len(df) % rows_per_page > 0) #ciel int
    nv = 1
    for i in range(0, nh):
        for j in range(0, nv):
            page = df.iloc[(i*rows_per_page):min((i+1)*rows_per_page, len(df)),
                           (j*cols_per_page):min((j+1)*cols_per_page, len(df.columns))]
            fig = _draw_as_table(page, pagesize)
            
            if nh > 1 or nv > 1:
                # Add a part/page number at bottom-center of page
                fig.text(0.5, 0.5/pagesize[0],
                         "Part-{}x{}: Page-{}".format(i+1, j+1, i*nv + j + 1),
                         ha='center', fontsize=8)
            plt.tight_layout()
            pdf.savefig(fig, bbox_inches='tight',pad_inches = 0.3,dpi = 300)
            
            plt.close()

def get_schedule_file():

    sqlstr = '''
        select
        a.rigid,
        b.slotdate,
        b.slottime,
        b.subjid,
        c.rfid,
        c.pyratid ,
        c.cageid,
        c.pyratcageid,
        c.owner,
        d.protocol,
        d.settingsname,
        b.comments,
        b.technotes
    from
        met.rig_view as a
    left join met.schedule as b on
        a.rigid = b.rigid
    left join met.animals as c on
            b.subjid = c.subjid 
    left join met.current_settings as d on
            b.subjid = d.subjid 
    where
        a.autorun = 1
        and b.slotdate = cast(current_timestamp() as date)
        and b.subjid REGEXP '.*-[MRP]-.*'
    order by 
        b.slottime asc,
        a.rigid asc

    '''

    schedule_df = readDB_SQL(sqlstr)
    # print(schedule_df)
    df_print = pd.DataFrame()
    df_print['Slot Time'] = schedule_df['slottime'].astype(str).str[-8:-3]
    for col in schedule_df.columns:
        schedule_df[col] = schedule_df[col].apply(lambda x: str(x) if x else '' )
    df_print['Rig'] = schedule_df['rigid']
    df_print['Cage(PyRatCageID)'] = schedule_df['cageid'].astype(str)+'('+schedule_df['pyratcageid'].apply(lambda x: str(x)[-3:])+')'
    df_print['SubjID(RFID)'] = schedule_df['subjid'].astype(str)+'('+schedule_df['rfid'].apply(lambda x: str(x)[-6:])+')'
    df_print['Owner/Protocol/Settings Name'] = schedule_df['owner'] + '/' + schedule_df['protocol'] + '/' + schedule_df['settingsname']
    df_print['Tech Notes'] = schedule_df['technotes']
    df_print['Comments'] = schedule_df['comments']

    dataframe_to_pdf(df_print,'./schedule.pdf')

    return './schedule.pdf'

def get_real_animal_list(pseudo_subj_id):
    """
    Get the list of real animals from the pseudo subj id. met.pseudo_subjects table 
    """
    sql_query = f'''
    select subjlist from met.pseudo_subject where subjid = '{pseudo_subj_id}'
    '''
    real_subj_list = readDB_SQL(sql_query)
    if len(real_subj_list) == 0:
        return []
    return real_subj_list['subjlist'].iloc[0].split(',')

def get_lick_position_subjid(subjid):
    sql_query = f'''
    select * from met.hf_lick_position where subjid = '{subjid}' order by ts desc limit 1
    '''
    subj_lick_position = readDB_SQL(sql_query)
    if subj_lick_position.empty:
        return 0
    else:
        return subj_lick_position['position'].iloc[0]
    
def update_hf_lick_position(subjid,lickposition,experid):
    sqlstr = f"""
    insert into met.hf_lick_position (subjid, position, experid) value ('{subjid}',{lickposition} ,{experid})
            """
    DBC.execute(sqlstr)

    
class zmq_messenger(object):
    zmq_conn = False
    zmq_pusher = ''
    
    def __init__(self):
        self.zmq_conn = False
        
    def getPusherConnection(self):
        self.zmq_pusher = zmqhelper.pusher()
        self.zmq_conn = True
        
    def rig_test(self,rigid):
        now = datetime.now()
        current_ts = now.strftime('%Y-%m-%d_%H:%M:%S')
        msg = json.dumps({'event':'rig_test','ts':current_ts})
        self.zmq_pusher.send_string(f'{rigid}_rigtest {msg}')
        
    def stop_rigtest(self,rigid):
        now = datetime.now()
        current_ts = now.strftime('%Y-%m-%d_%H:%M:%S')
        msg = json.dumps({'event':'stop_rigtest','ts':current_ts})
        self.zmq_pusher.send_string(f'{rigid}_stoprigtest {msg}')
        
    def stop_session(self, sessid, rigid):
        now = datetime.now()
        current_ts = now.strftime('%Y-%m-%d_%H:%M:%S')
        msg = json.dumps({'event':'stop_session','sessid':str(sessid),'ts':current_ts})
        self.zmq_pusher.send_string(f'{rigid}_stopsession {msg}')
        
    def start_session(self, subjid, rigid):
        now = datetime.now()
        current_ts = now.strftime('%Y-%m-%d_%H:%M:%S')
        msg = json.dumps({'event':'start_session','subjid':subjid,'ts':current_ts})
        self.zmq_pusher.send_string(f'{rigid}_startsession {msg}')
        

# %%
