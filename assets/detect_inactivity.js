

let timer, currSeconds = 0;


function resetTimer() {
    /* Hide the timer text */

    /* Clear the previous interval */
    clearInterval(timer);

    /* Reset the seconds of the timer */
    currSeconds = 0;

    /* Set a new interval */
    
    timer =
        setInterval(startIdleTimer, 1000);
}

// Define the events that
// would reset the timer
window.onload = resetTimer;
window.onmousedown = resetTimer;
window.ontouchstart = resetTimer;
window.onclick = resetTimer;
window.onmousemove = resetTimer;
window.onscroll = resetTimer;

function startIdleTimer() {

    /* Increment the
        timer seconds */
    currSeconds++;

    // after 2 hours of inactivity, log the user out
    if (currSeconds > 2 * 60 *60) {
        resetTimer();

        // after 2 hours of inactivity on the session expired page raise the alert
        if (window.location.href.includes("session_expired")) {
            // if the user is already on the session expired page, don't redirect them again
            // instead try closing the tab
            alert("Your session has expired. Please refresh the dash!");
            return;
        }

        window.location.href = '../session_expired';
        // window.console.log("User has been logged out for inactivity");
    }

}