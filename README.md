# rig-dashboard

This project was written using [Dash Plotly Python3](https://dash.plotly.com/)

`/` The home page is the rigs monitor, where you can check every rig's status and use the button on the upper right corner of each rig card to stop a session or start/ stop a rig test.

`/schedule` page displays the `subjids` in the rig/session table on the selected date, which can be changed using the datepicker on the upper left corner of this page. You can also double-click the table to modify the schedule and then click the `update schedule` button to save the changes to the database.

`/utility` page will include some basic functions, such as `start a session`..., and so on.


## Installation

First install [Miniconda](https://docs.conda.io/en/latest/miniconda.html) and [mamba](https://github.com/mamba-org/mamba).
then in a terminal run:
```
mamba install -f env.yml
```

```
Clone this reposistory
git clone https://gitlab.com/sainsbury-wellcome-centre/delab/devops/rig-dashboard.git

cd rig-dashboard

Clone the repository for helpers inside this
git clone https://gitlab.com/sainsbury-wellcome-centre/erlichlab/helpers

Configure the folowing in the ~/.dbconf file

[client]
DB Settings
[zmq]
zmq URL and Ports
```

## Development

### `debug`
run `python dash_rigs.py` in the terminal at the parent level

### `deployment`
`gunicorn dash_rigs:server --workers=4 -b:<port> --timeout 0`

_if the callback structure changed, before deploy, close all the webpages on the clients side or restart the nginx_
