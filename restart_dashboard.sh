#!/bin/bash

eval "$(command conda 'shell.bash' 'hook' 2> /dev/null)"

conda activate rig-dashboard

export PYTHONPATH=~/repos

PID=dashboard.pid
if [ -f $PID ]; then
    kill -9 `cat $PID`
    rm $PID
fi

gunicorn -w 4 dash_rigs:server --timeout 0 --pid $PID